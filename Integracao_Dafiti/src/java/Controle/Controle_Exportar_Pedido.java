/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controle;

import static Controle.Controle_Pedidos.getPedidoId;
import Dados.Cliente;
import Dados.Pedido;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Hericles
 */
public class Controle_Exportar_Pedido {
    public static Pedido pedido_enviar = new Pedido();
    
    public static int inserirPedidoERP(String id_pedido){
        
        // Data padrão para o cadastro dos clientes par os pedidos que vieram da dafiti - via marketplace
        String data_nascimento_padrao = "01/01/2001";
        
        // Pegando o pedido e seus dados
        // 1 - Pedido
        // 2 - Cliente
        // 3 - Endereço de Cobrança
        // 4 - Endereço de Entrega
        Pedido pedido = getPedidoId(id_pedido);
        
        //System.out.println(pedido.getNumero_pedido());
        //System.out.println(pedido.getPreco());
        //System.out.println(pedido.getDesconto());
        
        String cod_representante = "";
        String cod_tabela_preco = "";
        String tipo_documento = null;
        String tipo_bandeira = "";
        String data_previsao_baixa = "";//DD/MM/AAAA
        String tipo_cobranca = "18";
        String porcentagem_desconto = "0";
        
        // Pegar dados do cliente - Dafiti (Pedido)
        Cliente cliente = pedido.getCliente();
        
        // Verificar se ele já está cadastrado no virtual, se não estiver temos que cadastrar ele
        // Se estiver cadastrado verificar se houve alguma alteração em seus dados - Atualizar 
        
        
        //Data de criação do pedido
        //Metodo de pagamento
        
        
        //Consultar o código da operação
        int codigo_operacao = GetOperacao(pedido.getCliente().getEnd_entrega().getEndereco4(), pedido.getPreco());
        
        
        //Tipo de frete
        int tipo_frete = 1;
        
        if(Double.parseDouble(pedido.getFrete()) > 0){
            tipo_frete = 2;
        }
        
        System.out.println("Número do pedido: " + pedido.getNumero_pedido());
        
        
        //Método de pagamento
        String cod_pagamento;
        String observacao_pagamento = "";
        
        if("braspag_cc".equals(pedido.getMetodo_pagamento())){// Cartão de crédito
            //verificar o codigo
            cod_pagamento = "001";
            observacao_pagamento = "PAGAMENTO EFETUADO NO CARTÃO";
        }
        else if("braspag_boleto".equals(pedido.getMetodo_pagamento())){// Boleto
            cod_pagamento = "001";
            observacao_pagamento = "PAGAMENTO EFETUADO NO BOLETO";
        }
        
        
        //Observação de Pedido
        List<String> observacoes_pedido = new ArrayList<>();
        DecimalFormat df = new DecimalFormat("###,##0.00");
        
        observacoes_pedido.add("PAGAMENTO EFETUADO VIA MARKETPLACE");
        observacoes_pedido.add("ENVIAR PEDIDO POR PAC");
        observacoes_pedido.add("HIPPING COST : RS" + df.format(Double.parseDouble(pedido.getFrete())));
        
        //Coodigo de transportadora 
        String cod_transportadora = "5002";
        
        //Coleção de produtos que estão no pedido
        List<String> lista_colecoes = new ArrayList<>();
        
        
        //Setando variáveis 
        
        
        return 1;
    }
    
    
    public static int GetOperacao(String estado_cliente, String valor_pedido){
        String cod_operacao = "0";
        String estado_principal = "ES";
        
        if(estado_cliente.equals(estado_principal)){// se o estado for o ES
            cod_operacao = "346";
        }
        else if(Double.parseDouble(valor_pedido) > 0){// se o valor do pedido é maior que R$0,00
            cod_operacao = "380";
        }
        else if(!estado_cliente.equals(estado_principal)){// se o estado for diferente do ES
            cod_operacao = "347";
        }
        
        return Integer.parseInt(cod_operacao);
    }
}
