
package Controle;

import Dados.Pedido;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;

public class Gerar_PDF {
    Pedido pedido_selecionado = new Pedido();
    
    public void GerarPDF(Pedido ped) throws FileNotFoundException, BadElementException, IOException{
        Document document = new Document();
        
        pedido_selecionado = ped;
        
        try {
            PdfWriter.getInstance(document, new FileOutputStream("documento.pdf"));
            document.open();
            document = MontarCabecalho(document);
            
        } catch (DocumentException ex) {
            System.out.println("Error:"+ex);
        }finally{
            document.close();
        }
        
        try {
            Desktop.getDesktop().open(new File("documento.pdf"));
        } catch (IOException ex) {
            System.out.println("Error:"+ex);
        }
    }
    
    public Document MontarCabecalho(Document document) throws DocumentException, BadElementException, IOException{
        document.add(new Paragraph(new Phrase(20F, "                                                Detalhes do Pedido de N° " + pedido_selecionado.getId_pedido(), FontFactory.getFont(FontFactory.TIMES_BOLD, 13F))));

        document.add(new Paragraph(new Phrase(20F, "                   ", FontFactory.getFont(FontFactory.TIMES_BOLD, 13F))));
        document.add(new Paragraph("Ped:"));
        document.add(new Paragraph("Cod:"));
        document.add(new Paragraph("Transf:"));
        document.add(new Paragraph(new Phrase(20F, "                   ", FontFactory.getFont(FontFactory.TIMES_BOLD, 13F))));
        
        //__________________________________________________________________________________________________
        
        PdfPTable tabela1 =  new PdfPTable(2);

        PdfPCell titulo1 = new PdfPCell(new Paragraph(new Phrase(20F, "Cliente", FontFactory.getFont(FontFactory.TIMES_BOLD, 13F))));
        titulo1.setHorizontalAlignment(Element.ALIGN_CENTER);
        titulo1.setColspan(1);
        tabela1.addCell(titulo1);
        titulo1 = new PdfPCell(new Paragraph(new Phrase(20F, "Endereço", FontFactory.getFont(FontFactory.TIMES_BOLD, 13F))));
        titulo1.setHorizontalAlignment(Element.ALIGN_CENTER);
        titulo1.setColspan(1);
        tabela1.addCell(titulo1);
        PdfPCell e = new PdfPCell(new Paragraph("Data: " + pedido_selecionado.getData_criacao_pedido()));
        tabela1.addCell(e);
        e = new PdfPCell(new Paragraph(pedido_selecionado.getCliente().getEnd_cobranca().getEndereco1()));
        tabela1.addCell(e);
        e = new PdfPCell(new Paragraph("Cliente: " + pedido_selecionado.getCliente().getNome()));
        tabela1.addCell(e);
        e = new PdfPCell(new Paragraph(pedido_selecionado.getCliente().getEnd_cobranca().getEndereco2()));
        tabela1.addCell(e);
        e = new PdfPCell(new Paragraph("CPF: " + pedido_selecionado.getCliente().getCpf()));
        tabela1.addCell(e);
        e = new PdfPCell(new Paragraph(pedido_selecionado.getCliente().getEnd_cobranca().getEndereco3()));
        tabela1.addCell(e);
        e = new PdfPCell(new Paragraph("Telefone 1: " + pedido_selecionado.getCliente().getEnd_cobranca().getTelefone1()));
        tabela1.addCell(e);
        e = new PdfPCell(new Paragraph(pedido_selecionado.getCliente().getEnd_cobranca().getEndereco4()));
        tabela1.addCell(e);
        e = new PdfPCell(new Paragraph("Telefone 2: " + pedido_selecionado.getCliente().getEnd_cobranca().getTelefone2()));
        tabela1.addCell(e);
        e = new PdfPCell(new Paragraph(pedido_selecionado.getCliente().getEnd_cobranca().getEndereco5()));
        tabela1.addCell(e);
        
        if("braspag_cc".equals(pedido_selecionado.getMetodo_pagamento())){
            e = new PdfPCell(new Paragraph("Pagamento: Cartão"));
            tabela1.addCell(e);
        }
        else if("braspag_boleto".equals(pedido_selecionado.getMetodo_pagamento())){
            e = new PdfPCell(new Paragraph("Pagamento: Boleto"));
            tabela1.addCell(e);     
        }
        
        e = new PdfPCell(new Paragraph(pedido_selecionado.getCliente().getEnd_entrega().getCidade() + " " + pedido_selecionado.getCliente().getEnd_cobranca().getCod_postal() + " " + pedido_selecionado.getCliente().getEnd_cobranca().getPais()));
        tabela1.addCell(e);
        
        document.add(tabela1);
        
        //____________________________________________________________________________________________________________

        document.add(new Paragraph(new Phrase(20F, "                   ", FontFactory.getFont(FontFactory.TIMES_BOLD, 13F))));

        PdfPTable tabela =  new PdfPTable(6);
        tabela.getDefaultCell().setBorder(PdfPCell.NO_BORDER);

        PdfPCell titulo = new PdfPCell(new Paragraph("Produtos"));
        titulo.setHorizontalAlignment(Element.ALIGN_CENTER);
        titulo.setColspan(6);
        tabela.addCell(titulo);
        PdfPCell c1 = new PdfPCell(new Paragraph("#"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabela.addCell(c1);
        PdfPCell c2 = new PdfPCell(new Paragraph("Nome"));
        c2.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabela.addCell(c2);
        PdfPCell c3 = new PdfPCell(new Paragraph("Seller SKU"));
        c3.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabela.addCell(c3);
        PdfPCell c4 = new PdfPCell(new Paragraph("Dafiti SKU"));
        c4.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabela.addCell(c4);
        PdfPCell c5 = new PdfPCell(new Paragraph("Preço"));
        c5.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabela.addCell(c5);
        PdfPCell c6 = new PdfPCell(new Paragraph("Preço Pago"));
        c6.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabela.addCell(c6);
        
        for(int i = 0; i < pedido_selecionado.getProdutos().size(); i++){
            PdfPCell a1 = new PdfPCell(new Paragraph(Integer.toString(i + 1)));
            a1.setHorizontalAlignment(Element.ALIGN_CENTER);
            tabela.addCell(a1);
            PdfPCell a2 = new PdfPCell(new Paragraph(pedido_selecionado.getProdutos().get(i).getNome()));
            a2.setHorizontalAlignment(Element.ALIGN_CENTER);
            tabela.addCell(a2);
            PdfPCell a3 = new PdfPCell(new Paragraph(pedido_selecionado.getProdutos().get(i).getSku()));
            a3.setHorizontalAlignment(Element.ALIGN_CENTER);
            tabela.addCell(a3);
            PdfPCell a4 = new PdfPCell(new Paragraph(pedido_selecionado.getProdutos().get(i).getSku_dafiti()));
            a4.setHorizontalAlignment(Element.ALIGN_CENTER);
            tabela.addCell(a4);
            PdfPCell a5 = new PdfPCell(new Paragraph(pedido_selecionado.getProdutos().get(i).getPreco_original()));
            a5.setHorizontalAlignment(Element.ALIGN_CENTER);
            tabela.addCell(a5);
            PdfPCell a6 = new PdfPCell(new Paragraph(pedido_selecionado.getProdutos().get(i).getPreco_pago()));
            a6.setHorizontalAlignment(Element.ALIGN_CENTER);
            tabela.addCell(a6);
        }
        
        DecimalFormat df = new DecimalFormat("###,##0.00");
        double sub_total = Double.parseDouble(pedido_selecionado.getPreco()) - Double.parseDouble(pedido_selecionado.getFrete());
        double total = Double.parseDouble(pedido_selecionado.getPreco()) - Double.parseDouble(pedido_selecionado.getDesconto());
        double desconto =  Double.parseDouble(pedido_selecionado.getDesconto());

        PdfPCell s = new PdfPCell(new Paragraph("SubTotal"));
        s.setHorizontalAlignment(Element.ALIGN_CENTER);
        s.setColspan(5);
        tabela.addCell(s);
        s = new PdfPCell(new Paragraph(String.valueOf(df.format(sub_total))));
        s.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabela.addCell(s);
        s = new PdfPCell(new Paragraph("Desconto"));
        s.setHorizontalAlignment(Element.ALIGN_CENTER);
        s.setColspan(5);
        tabela.addCell(s);
        s = new PdfPCell(new Paragraph(String.valueOf(df.format(desconto))));
        s.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabela.addCell(s);
        s = new PdfPCell(new Paragraph("Frete"));
        s.setHorizontalAlignment(Element.ALIGN_CENTER);
        s.setColspan(5);
        tabela.addCell(s);
        s = new PdfPCell(new Paragraph(String.valueOf(df.format(Double.parseDouble(pedido_selecionado.getFrete())))));
        s.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabela.addCell(s);
        s = new PdfPCell(new Paragraph("Total"));
        s.setHorizontalAlignment(Element.ALIGN_CENTER);
        s.setColspan(5);
        tabela.addCell(s);
        s = new PdfPCell(new Paragraph(String.valueOf(df.format(total))));
        s.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabela.addCell(s);

        document.add(tabela);
        
        return document;
    }
}
