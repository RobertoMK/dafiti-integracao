
package Conexao_Dafiti;

import static Conexao_Dafiti.SellerCenterAPI.getCurrentTimestamp;
import static Conexao_Dafiti.SellerCenterAPI.getSellercenterApiResponse;
import java.util.HashMap;
import java.util.Map;

public class Conexao {
    
    public static String getConnectionOrders(){
        Map<String, String> params = new HashMap<>();
        
        params.put("UserID", "paulo@pwbrasil.ind.br");
        params.put("Timestamp", getCurrentTimestamp());
        params.put("Version", "1.0");
        params.put("Action", "GetOrders");
        //params.put("Limit","1");/*parametros de consulta também vão nesse map*/
        
        final String apiKey = "d8404a5f5793bfe50a8e9ffbe8141bf5b67cf904";
        final String XML = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?><Request></Request>";//no XML se insere informações de cadastro, caso seja necessário
        final String out = getSellercenterApiResponse(params, apiKey, XML); 
        
        return out; 
    }
    
    public static String getConnectionOrderItems(String order_id){
        Map<String, String> params = new HashMap<>();
        
        params.put("UserID", "paulo@pwbrasil.ind.br");
        params.put("Timestamp", getCurrentTimestamp());
        params.put("Version", "1.0");
        params.put("Action", "GetOrderItems");
        params.put("OrderId", order_id);/*parametros de consulta também vão nesse map*/
        
        final String apiKey = "d8404a5f5793bfe50a8e9ffbe8141bf5b67cf904";
        final String XML = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?><Request></Request>";//no XML se insere informações de cadastro, caso seja necessário
        final String out = getSellercenterApiResponse(params, apiKey, XML); 
        
        return out; 
    }
}
