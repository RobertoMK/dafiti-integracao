
package Dados;

import java.util.ArrayList;
import java.util.List;

public class Pedido {
    private String primeiro_nome;
    private String segundo_nome;
    private String id_pedido;
    private String numero_pedido;
    private String metodo_pagamento;
    private String observacao_pedido;
    private String informacao_entrega;
    private String preco;
    private String op_presente;// 1 - Presente  /  0 - se não for presente
    private String text_presente;// texto se o pedido for um presente
    private String data_criacao_pedido;
    private String data_ultima_alteracao_pedido;
    private String qtd_itens;
    private String tempo_envio;
    private String atributos_extras;//cor e informações do pedido
    private String status;
    private String desconto;
    private String frete;
    private Cliente cliente;
    private List<Produto> produtos = new ArrayList<>();

    public Pedido() {
    }
    
    public String getId_pedido() {
        return id_pedido;
    }

    public void setId_pedido(String id_pedido) {
        this.id_pedido = id_pedido;
    }

    public String getNumero_pedido() {
        return numero_pedido;
    }

    public void setNumero_pedido(String numero_pedido) {
        this.numero_pedido = numero_pedido;
    }

    public String getMetodo_pagamento() {
        return metodo_pagamento;
    }

    public void setMetodo_pagamento(String metodo_pagamento) {
        this.metodo_pagamento = metodo_pagamento;
    }

    public String getObservacao_pedido() {
        return observacao_pedido;
    }

    public void setObservacao_pedido(String observacao_pedido) {
        this.observacao_pedido = observacao_pedido;
    }

    public String getInformacao_entrega() {
        return informacao_entrega;
    }

    public void setInformacao_entrega(String informacao_entrega) {
        this.informacao_entrega = informacao_entrega;
    }

    public String getPreco() {
        return preco;
    }

    public void setPreco(String preco) {
        this.preco = preco;
    }

    public String getOp_presente() {
        return op_presente;
    }

    public void setOp_presente(String op_presente) {
        this.op_presente = op_presente;
    }

    public String getText_presente() {
        return text_presente;
    }

    public void setText_presente(String text_presente) {
        this.text_presente = text_presente;
    }

    public String getData_criacao_pedido() {
        return data_criacao_pedido;
    }

    public void setData_criacao_pedido(String data_criacao_pedido) {
        this.data_criacao_pedido = data_criacao_pedido;
    }

    public String getData_ultima_alteracao_pedido() {
        return data_ultima_alteracao_pedido;
    }

    public void setData_ultima_alteracao_pedido(String data_ultima_alteracao_pedido) {
        this.data_ultima_alteracao_pedido = data_ultima_alteracao_pedido;
    }

    public String getQtd_itens() {
        return qtd_itens;
    }

    public void setQtd_itens(String qtd_itens) {
        this.qtd_itens = qtd_itens;
    }

    public String getTempo_envio() {
        return tempo_envio;
    }

    public void setTempo_envio(String tempo_envio) {
        this.tempo_envio = tempo_envio;
    }

    public String getAtributos_extras() {
        return atributos_extras;
    }

    public void setAtributos_extras(String atributos_extras) {
        this.atributos_extras = atributos_extras;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public List<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(List<Produto> produtos) {
        this.produtos = produtos;
    }

    public String getPrimeiro_nome() {
        return primeiro_nome;
    }

    public void setPrimeiro_nome(String primeiro_nome) {
        this.primeiro_nome = primeiro_nome;
    }

    public String getSegundo_nome() {
        return segundo_nome;
    }

    public void setSegundo_nome(String segundo_nome) {
        this.segundo_nome = segundo_nome;
    }

    public String getDesconto() {
        return desconto;
    }

    public void setDesconto(String desconto) {
        this.desconto = desconto;
    }

    public String getFrete() {
        return frete;
    }

    public void setFrete(String frete) {
        this.frete = frete;
    }
    
    
    
}
