
package Dados;

public class Produto {
    private String id_produto_pedido;//order item id
    private String id_dafiti;//shop id
    private String id_pedido;//order id
    private String sku;
    private String sku_dafiti;
    private String nome;
    private String marca;
    private String descricao;
    private String preco_original;//item price
    private String preco_pago;//praid price
    private String moeda;//currency
    private String imposto;//tax amount
    private String frete;//shipping amount
    private String voucher;//voucher code
    private String voucher_amount;//voucher code
    private String status;
    private String tipo_envio;
    private String transportadora;// PAC correios // ShipmentProvider

    public Produto() {
    }

    public String getId_produto_pedido() {
        return id_produto_pedido;
    }

    public void setId_produto_pedido(String id_produto_pedido) {
        this.id_produto_pedido = id_produto_pedido;
    }

    public String getId_dafiti() {
        return id_dafiti;
    }

    public void setId_dafiti(String id_dafiti) {
        this.id_dafiti = id_dafiti;
    }

    public String getId_pedido() {
        return id_pedido;
    }

    public void setId_pedido(String id_pedido) {
        this.id_pedido = id_pedido;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getSku_dafiti() {
        return sku_dafiti;
    }

    public void setSku_dafiti(String sku_dafiti) {
        this.sku_dafiti = sku_dafiti;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getPreco_original() {
        return preco_original;
    }

    public void setPreco_original(String preco_original) {
        this.preco_original = preco_original;
    }

    public String getPreco_pago() {
        return preco_pago;
    }

    public void setPreco_pago(String preco_pago) {
        this.preco_pago = preco_pago;
    }

    public String getMoeda() {
        return moeda;
    }

    public void setMoeda(String moeda) {
        this.moeda = moeda;
    }

    public String getImposto() {
        return imposto;
    }

    public void setImposto(String imposto) {
        this.imposto = imposto;
    }

    public String getFrete() {
        return frete;
    }

    public void setFrete(String frete) {
        this.frete = frete;
    }

    public String getVoucher() {
        return voucher;
    }

    public void setVoucher(String voucher) {
        this.voucher = voucher;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTransportadora() {
        return transportadora;
    }

    public void setTransportadora(String transportadora) {
        this.transportadora = transportadora;
    }

    public String getVoucher_amount() {
        return voucher_amount;
    }

    public void setVoucher_amount(String voucher_amount) {
        this.voucher_amount = voucher_amount;
    }

    public String getTipo_envio() {
        return tipo_envio;
    }

    public void setTipo_envio(String tipo_envio) {
        this.tipo_envio = tipo_envio;
    }
    
//    System.out.println("Item " + (i + 1) + ": ");
//    System.out.println("\tOrderItemId: " + element.getElementsByTagName("OrderItemId").item(i).getTextContent());//dessa forma se pega a string dentro das tags
//    System.out.println("\tShopId: " + element.getElementsByTagName("ShopId").item(i).getTextContent());
//    System.out.println("\tOrderId: " + element.getElementsByTagName("OrderId").item(i).getTextContent());
//    System.out.println("\tName: " + element.getElementsByTagName("Name").item(i).getTextContent());
//    System.out.println("\tSku: " + element.getElementsByTagName("Sku").item(i).getTextContent());
//    System.out.println("\tShopSku: " + element.getElementsByTagName("ShopSku").item(i).getTextContent());
//    System.out.println("\tShippingType: " + element.getElementsByTagName("ShippingType").item(i).getTextContent());
//    System.out.println("\tItemPrice: " + element.getElementsByTagName("ItemPrice").item(i).getTextContent());
//    System.out.println("\tPaidPrice: " + element.getElementsByTagName("PaidPrice").item(i).getTextContent());
//    System.out.println("\tCurrency: " + element.getElementsByTagName("Currency").item(i).getTextContent());
//    System.out.println("\tWalletCredits: " + element.getElementsByTagName("WalletCredits").item(i).getTextContent());
//    System.out.println("\tTaxAmount: " + element.getElementsByTagName("TaxAmount").item(i).getTextContent());
//    System.out.println("\tShippingAmount:" + element.getElementsByTagName("ShippingAmount").item(i).getTextContent());
//    System.out.println("\tVoucherAmount: " + element.getElementsByTagName("VoucherAmount").item(i).getTextContent());
//    System.out.println("\tVoucherCode: " + element.getElementsByTagName("VoucherCode").item(i).getTextContent());
//    System.out.println("\tStatus: " + element.getElementsByTagName("Status").item(i).getTextContent());
//    System.out.println("\tIsProcessable: " + element.getElementsByTagName("IsProcessable").item(i).getTextContent());
//    System.out.println("\tShipmentProvider: " + element.getElementsByTagName("ShipmentProvider").item(i).getTextContent());
//    System.out.println("\tIsDigital: " + element.getElementsByTagName("IsDigital").item(i).getTextContent());
//    System.out.println("\tDigitalDeliveryInfo: " + element.getElementsByTagName("DigitalDeliveryInfo").item(i).getTextContent());
//    System.out.println("\tTrackingCode: " + element.getElementsByTagName("TrackingCode").item(i).getTextContent());
//    System.out.println("\tReason: " + element.getElementsByTagName("Reason").item(i).getTextContent());
//    System.out.println("\tReasonDetail: " + element.getElementsByTagName("ReasonDetail").item(i).getTextContent());
//    System.out.println("\tPurchaseOrderId:" + element.getElementsByTagName("PurchaseOrderId").item(i).getTextContent());
//    System.out.println("\tPurchaseOrderNumber: " + element.getElementsByTagName("PurchaseOrderNumber").item(i).getTextContent());
//    System.out.println("\tPackageId: " + element.getElementsByTagName("PackageId").item(i).getTextContent());
//    System.out.println("\tExtraAttributes: " + element.getElementsByTagName("ExtraAttributes").item(i).getTextContent());
//    System.out.println("\tCreatedAt: " + element.getElementsByTagName("CreatedAt").item(i).getTextContent());
//    System.out.println("\tUpdatedAt: " + element.getElementsByTagName("UpdatedAt").item(i).getTextContent());
//    System.out.println("\tReturnStatus : " + element.getElementsByTagName("ReturnStatus").item(i).getTextContent());
    
}
