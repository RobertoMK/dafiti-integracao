
package Dados;

public class Endereco{
    private String primeiro_nome;
    private String ultimo_nome;
    private String telefone1;
    private String telefone2;
    private String endereco1;
    private String endereco2;
    private String endereco3;
    private String endereco4;
    private String endereco5;
    private String email;
    private String cidade;
    private String cod_postal;
    private String pais;
    private String regiao;

    public Endereco() {
    }
    
    public String getPrimeiro_nome() {
        return primeiro_nome;
    }

    public void setPrimeiro_nome(String primeiro_name) {
        this.primeiro_nome = primeiro_name;
    }

    public String getUltimo_nome() {
        return ultimo_nome;
    }

    public void setUltimo_nome(String ultimo_name) {
        this.ultimo_nome = ultimo_name;
    }

    public String getTelefone1() {
        return telefone1;
    }

    public void setTelefone1(String telefone1) {
        this.telefone1 = telefone1;
    }

    public String getTelefone2() {
        return telefone2;
    }

    public void setTelefone2(String telefone2) {
        this.telefone2 = telefone2;
    }

    public String getEndereco1() {
        return endereco1;
    }

    public void setEndereco1(String endereco1) {
        this.endereco1 = endereco1;
    }

    public String getEndereco2() {
        return endereco2;
    }

    public void setEndereco2(String endereco2) {
        this.endereco2 = endereco2;
    }

    public String getEndereco3() {
        return endereco3;
    }

    public void setEndereco3(String endereco3) {
        this.endereco3 = endereco3;
    }

    public String getEndereco4() {
        return endereco4;
    }

    public void setEndereco4(String endereco4) {
        this.endereco4 = endereco4;
    }

    public String getEndereco5() {
        return endereco5;
    }

    public void setEndereco5(String endereco5) {
        this.endereco5 = endereco5;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getCod_postal() {
        return cod_postal;
    }

    public void setCod_postal(String cod_postal) {
        this.cod_postal = cod_postal;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getRegiao() {
        return regiao;
    }

    public void setRegiao(String regiao) {
        this.regiao = regiao;
    }
    
    
    
}


