
package Conexao_Virtual_Age;

import Conexao_Dafiti.Conexao;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.xml.sax.SAXException;

public class Conexao_VA {
    //Interno
    //http://192.168.200.240:80/wbsStoreage/VirtualWsServer.exe/wsdl/IdmDados
    
    //Externo
    static String url_1 = "https://www.bhan.com.br/wbsStoreage/VirtualWsServer.exe/soap/IdmDados";
    static String url_2 = "https://www.bhan.com.br/wbsStoreage/VirtualWsServer.exe/wsdl/IdmDados";
    
    
    public static void main(String[] args) throws IOException, MalformedURLException, ParserConfigurationException, SAXException, TransformerException {
        try{
            //<produto acao="con" cd_produto="50"/>
            getERPCliente("<produto acao='con' cd_produto='50' />");
            
        }catch (IOException ex){
            System.out.println("Erro ao conectar com o Cliente");
            Logger.getLogger(Conexao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static String getERPCliente(String action) throws MalformedURLException, IOException{
        URL url = new URL(url_2);
        HttpURLConnection connection = null;
        String output = "";
        
        connection = (HttpURLConnection) url.openConnection();
        connection.setDoOutput(true);
        connection.setDoInput(true);
        //connection.setReadTimeout(30000);
        connection.setRequestMethod("GET");
        connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        connection.setRequestProperty("charset", "UTF-8");
        
        final String XML = "<?xml version='1.0' encoding='utf-8'?>"
                            + "<requisicao in_schema='F' >"
                                +"<loginws cd_loginws='pwbrasil' cd_senhaws='987654' />"
                                    +action
                            +"</requisicao>";
        
        connection.setRequestProperty("Content-Length", "" + Integer.toString(XML.getBytes().length));
        try (DataOutputStream wr = new DataOutputStream(connection.getOutputStream())) {
            wr.writeBytes(XML);
            wr.flush();
        }
                
        String line;
        BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        while ((line = reader.readLine()) != null) {
            output += line + "\n";
        }
        
        System.out.println(output);
        return output;
    }   
}
