
package Dados;

public class Cliente {
    private String cpf;
    private String nome;
    private Endereco end_cobranca;//Billing
    private Endereco end_entrega;//Shipping

    public Cliente() {
    }
    
    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Endereco getEnd_cobranca() {
        return end_cobranca;
    }

    public void setEnd_cobranca(Endereco end_cobranca) {
        this.end_cobranca = end_cobranca;
    }

    public Endereco getEnd_entrega() {
        return end_entrega;
    }

    public void setEnd_entrega(Endereco end_entrega) {
        this.end_entrega = end_entrega;
    }
}
