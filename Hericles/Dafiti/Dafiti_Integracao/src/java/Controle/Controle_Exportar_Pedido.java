
package Controle;

import static Controle.Controle_Pedidos.getPedidoId;
import Dados.Cliente;
import Dados.Pedido;
import Dados.Produto;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class Controle_Exportar_Pedido {
    public static SimpleDateFormat df;
    public static Pedido pedido_selecionado = new Pedido();
    public static List<Produto> produtos_vb = new ArrayList<>();
    public static List<Produto> produtos_m = new ArrayList<>();
    public static int dividir_pedido = 0;
    
    public static int inserirPedidoERP(String id_pedido){
        
        // Data padrão para o cadastro dos clientes par os pedidos que vieram da dafiti - via marketplace
        String data_nascimento_padrao = "01/01/2001";
        
        // Pegando o pedido e seus dados
        // 1 - Pedido (Cliente, Endereço de Cobrança, Endereço de Entrega)
        pedido_selecionado = getPedidoId(id_pedido);
        
        String cod_representante = "";
        String cod_tabela_preco = "";
        String tipo_documento = null;
        
        //Data Atual
        Date data = new Date();
        df = new SimpleDateFormat("dd/MM/yyyy");
        String data_previsao_baixa = df.format(data);
        
        String tipo_cobranca = "18";
        String porcentagem_desconto = "0";
        
        // Pegar dados do cliente - Dafiti (Pedido)
        Cliente cliente = pedido_selecionado.getCliente();
        
        // Verificar se ele já está cadastrado no virtual, se não estiver temos que cadastrar ele
        // Se estiver cadastrado verificar se houve alguma alteração em seus dados - Atualizar 
        
        
        //Consultar o código da operação
        String codigo_operacao = GetOperacao(pedido_selecionado.getCliente().getEnd_entrega().getEndereco4(), pedido_selecionado.getPreco());
        
        
        //Tipo de frete
        String tipo_frete = "1";
        
        if(Double.parseDouble(pedido_selecionado.getFrete()) > 0){
            tipo_frete = "2";
        }
        
        //Método de pagamento
        String cod_pagamento = null;
        String observacao_pagamento = "";
        
        if("braspag_cc".equals(pedido_selecionado.getMetodo_pagamento())){// Cartão de crédito
            //verificar o codigo
            cod_pagamento = "001";
            observacao_pagamento = "PAGAMENTO EFETUADO NO CARTÃO";
        }
        else if("braspag_boleto".equals(pedido_selecionado.getMetodo_pagamento())){// Boleto
            cod_pagamento = "001";
            observacao_pagamento = "PAGAMENTO EFETUADO NO BOLETO";
        }
        
        
        //Observação de Pedido
        List<String> observacoes_pedido = new ArrayList<>();
        DecimalFormat df1 = new DecimalFormat("###,##0.00");
        
        observacoes_pedido.add("PAGAMENTO EFETUADO VIA MARKETPLACE");
        observacoes_pedido.add("ENVIAR PEDIDO POR PAC");
        observacoes_pedido.add("SHIPPING COST : RS" + df1.format(Double.parseDouble(pedido_selecionado.getFrete())));
        
        String nome_transporte = "E. C. T. CORREIOS";
        
        
        //Coodigo de transportadora 
        String cod_transportadora = "5002";
        
        
        //Pega as coleções, tamanhos e lojas (MB ou VB) dos produtos que estão no pedido
        GetOperacao_Tamanho_Loja(pedido_selecionado);
        
        //----------------------------------------------------------------------------------------------
        //Setando variáveis
        
        String v_tipo_frete = tipo_frete;
        String v_cod_cliente = null;// pegar codigo do cliente no ERP
        String v_cod_representante = null;// pegar no ERP
        String v_cod_pagamento = cod_pagamento;
        String v_data_chegada = pedido_selecionado.getData_criacao_pedido();
        String v_porcentagem_desconto = porcentagem_desconto;
        String v_tipo_cobranca = tipo_cobranca;
        String v_cod_tabela_preco = null;// pegar no ERP
        String v_data_previsao_baixa = data_previsao_baixa;
        String v_cod_operacao = codigo_operacao;
        String v_id_pedido = id_pedido;
        String v_cod_transporte = cod_transportadora;
        String v_nome_transporte = nome_transporte;
        String v_order_increment_id = null;// Falta pegar ainda
        String v_observacao_pagamento = observacao_pagamento;
        List<String> v_observacoes_pedido = observacoes_pedido;
        String cod_empresa = "006";
        
        //----------------------------------------------------------------------------------------------
        
        System.out.println("Tipo de Frete: " + v_tipo_frete);
        System.out.println("Codigo Cliente: " + v_cod_cliente);
        System.out.println("Codigo Representante: " + v_cod_representante);
        System.out.println("Data Pedido: " + v_data_chegada);
        System.out.println("Porcentagem Desconto: " + v_porcentagem_desconto);
        System.out.println("Tipo de Cobrança: " + v_tipo_cobranca);
        System.out.println("Codigo Pagamento: " + v_cod_pagamento);
        System.out.println("Observacao Pagamento: " + v_observacao_pagamento);
        System.out.println("Tabela de Preço: " + v_cod_tabela_preco);
        System.out.println("Data Baixa: " + v_data_previsao_baixa);
        System.out.println("Codigo Operação: " + v_cod_operacao);
        System.out.println("Id Pedido: " + v_id_pedido);
        System.out.println("Codigo Transporte: " + v_cod_transporte);
        System.out.println("Nome Transporte: " + v_nome_transporte);
        System.out.println("Id Increment: " + v_order_increment_id);
        System.out.println("Tipo de Frete: " + v_observacoes_pedido);
        
        //JOptionPane.showMessageDialog(null, "Pedido importado !!!");
        return 1;
    }
    
    
    public static String GetOperacao(String estado_cliente, String valor_pedido){
        String cod_operacao = "0";
        String estado_principal = "ES";
        
        if(estado_cliente.equals(estado_principal)){// se o estado for o ES
            cod_operacao = "346";
        }
        else if(Double.parseDouble(valor_pedido) > 0){// se o valor do pedido é maior que R$0,00
            cod_operacao = "380";
        }
        else if(!estado_cliente.equals(estado_principal)){// se o estado for diferente do ES
            cod_operacao = "347";
        }
        
        return cod_operacao;
    }
    
    public static void GetOperacao_Tamanho_Loja(Pedido p){
        String a, colecao_final = "";
        
        //Percorre todos os produtos
        for(int i = 0; i < pedido_selecionado.getProdutos().size(); i++){
            a = pedido_selecionado.getProdutos().get(i).getSku();

            String[] textoSeparado = a.split("/");   

            String j = textoSeparado[0];
            String numero = "";
            String letra = "";

            for(int x = 0; x < j.length(); x++){
                char c = j.charAt(x);
                if(Character.isLetter(c)){// se a posição for uma letra
                    //System.out.println("String");
                    letra = letra + (c);
                }
                else if(Character.isDigit(c)){// se a posição for numero
                    //System.out.println("Numero");
                    numero = numero + (c);
                }
            }

            char[] colecao = numero.toCharArray();
            
            int resto = (numero.length() - 5);
            
            for(int x = 0; x < resto; x++){
                //System.out.println(colecao[((resto) - (x + 1))]);
                colecao_final = colecao_final + colecao[((resto) - (x + 1))];
            }
          
            StringBuffer sb = new StringBuffer(colecao_final);
            sb.reverse();
            //System.out.println(sb);
            String u = sb.toString();
            System.out.println(sb);
            pedido_selecionado.getProdutos().get(i).setColecao(u);
            pedido_selecionado.getProdutos().get(i).setTamanho(textoSeparado[1]);
            colecao_final = "";
            numero = "";
            
            //System.out.println(letra);
            switch (Character.toString(letra.charAt(0))) {
                case "V":
                    String loja = Character.toString(letra.charAt(0)) + Character.toString(letra.charAt(1));
                    pedido_selecionado.getProdutos().get(i).setLoja(loja);
                    produtos_vb.add(pedido_selecionado.getProdutos().get(i));
                    break;
                case "M":
                    pedido_selecionado.getProdutos().get(i).setLoja(Character.toString(letra.charAt(0)));
                    produtos_m.add(pedido_selecionado.getProdutos().get(i));
                    break;      
            }      
        }
        
        if((produtos_vb.size() > 0) && (produtos_m.size() > 0)){
            dividir_pedido = 1;
        }
    }
}
