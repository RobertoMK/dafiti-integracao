/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controle;

import Conexao_Dafiti.Conexao;
import Dados.Cliente;
import Dados.Endereco;
import Dados.Pedido;
import Dados.Produto;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/**
 *
 * @author Hericles
 */
public class Controle_Pedidos extends HttpServlet{
    public static double desconto_;
    public static double frete_;
    public static SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/yy");
    public static int PEDIDOS_QTD = 100;

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response){
        try {
            response.setContentType("text/html;charset=UTF-8");
            PrintWriter out = response.getWriter();
            
            getPedidos();
            
        } catch (IOException s){
            System.out.println("Erro de escrita/Leitura");
        }
    }
    
    //Pronto
    public static Pedido getPedidoId(String id_pedido){
        List<Pedido> pedidos = new ArrayList<>();
        pedidos = getPedidos();
        
        for (int i = 0; i < pedidos.size(); i++){ //deve-se percorrer a Node list de itens que foi gerada acima para pegar os elementos. P.S.: forEach não funciona com NodeList
            if(pedidos.get(i).getId_pedido().equals(id_pedido)){
                return pedidos.get(i);
            }
        }
        
        return null;
    }
    
    //Pronto
    public static List<Pedido> getPedidos(){
        String out = Conexao.getConnectionOrders();
        NodeList items = null;
        List<Pedido> pedidos = new ArrayList<>();
        Pedido aux = new Pedido();
        int cont = 0;
        
        try{  
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            InputSource source = new InputSource();
            source.setCharacterStream(new StringReader(out)); //inseri a string em um InputSource pois o método parse estava com problemas para ler a string
            Document info = builder.parse(source);
            info.getDocumentElement().normalize(); //usei para confirmar a estrutura dos Nodes dentro da variável, é opcional
            items = info.getElementsByTagName("Order"); //puxo tudo que tem a tag de pedido dentro do XML e guardo em uma lista
            
            for (int i = (items.getLength() - 1); i > 0; i--){ //deve-se percorrer a Node list de itens que foi gerada acima para pegar os elementos. P.S.: forEach não funciona com NodeList
                if(cont < PEDIDOS_QTD){
                    Node nodeItem = items.item(i);

                    if (nodeItem.getNodeType() == Node.ELEMENT_NODE){
                        Element element = (Element) nodeItem;

                        pedidos.add(retornar_Pedido(element));

                    }
                    cont++;
                }
                
            }
        }catch(Exception e){ //preguiça de tratar as exceções xDDDDDDD
            System.out.println(e);
        }
        
        return pedidos;
    }
    
    //Pronto
    public static Pedido retornar_Pedido(Element element){
        Pedido aux = new Pedido();
        List<Produto> p = new ArrayList<>();// buscar pedidos
        
        aux.setPrimeiro_nome(element.getElementsByTagName("CustomerFirstName").item(0).getTextContent());
        aux.setSegundo_nome(element.getElementsByTagName("CustomerLastName").item(0).getTextContent());
        aux.setId_pedido(element.getElementsByTagName("OrderId").item(0).getTextContent());
        aux.setNumero_pedido(element.getElementsByTagName("OrderNumber").item(0).getTextContent());
        aux.setMetodo_pagamento(element.getElementsByTagName("PaymentMethod").item(0).getTextContent());
        aux.setObservacao_pedido(element.getElementsByTagName("Remarks").item(0).getTextContent());
        aux.setInformacao_entrega(element.getElementsByTagName("DeliveryInfo").item(0).getTextContent());
        aux.setPreco(element.getElementsByTagName("Price").item(0).getTextContent());
        aux.setOp_presente(element.getElementsByTagName("GiftOption").item(0).getTextContent());
        aux.setText_presente(element.getElementsByTagName("GiftMessage").item(0).getTextContent());
        aux.setData_criacao_pedido(element.getElementsByTagName("CreatedAt").item(0).getTextContent());
        aux.setData_ultima_alteracao_pedido(element.getElementsByTagName("UpdatedAt").item(0).getTextContent());
        aux.setQtd_itens(element.getElementsByTagName("ItemsCount").item(0).getTextContent());
        aux.setTempo_envio(element.getElementsByTagName("PromisedShippingTime").item(0).getTextContent());
        aux.setAtributos_extras(element.getElementsByTagName("ExtraAttributes").item(0).getTextContent());
        aux.setStatus(element.getElementsByTagName("Status").item(0).getTextContent());
        aux.setCliente(retornar_Cliente(element));
        aux.setProdutos(getItensPedido(element.getElementsByTagName("OrderId").item(0).getTextContent()));
        
        //System.out.println(element.getElementsByTagName("DeliveryInfo").item(0).getTextContent());
        
        aux.setDesconto(String.valueOf(desconto_));
        aux.setFrete(String.valueOf(frete_));
        //aux.setPreco(String.valueOf((Double.parseDouble(aux.getPreco())) - desconto_));
        return aux;
    }
    
    //Pronto
    public static Cliente retornar_Cliente(Element element){
        Cliente c = new Cliente();// buscar cliente
        
        c.setCpf(element.getElementsByTagName("NationalRegistrationNumber").item(0).getTextContent());
        c.setNome(element.getElementsByTagName("CustomerFirstName").item(0).getTextContent() + " " + element.getElementsByTagName("CustomerLastName").item(0).getTextContent());
        c.setEnd_cobranca(retornar_Endereco(element, 0));//Billing
        c.setEnd_entrega(retornar_Endereco(element, 1));//Shipping
        
        return c;
    }
    
    //Pronto
    public static Endereco retornar_Endereco(Element element, int position){
        Endereco aux = new Endereco();
        
        if(position == 0){//endereço de cobrança  - AddressBilling
            aux.setPrimeiro_nome(element.getElementsByTagName("FirstName").item(0).getTextContent());
            aux.setUltimo_nome(element.getElementsByTagName("LastName").item(0).getTextContent());
            aux.setTelefone1(element.getElementsByTagName("Phone").item(0).getTextContent());
            aux.setTelefone2(element.getElementsByTagName("Phone2").item(0).getTextContent());
            aux.setEndereco1(element.getElementsByTagName("Address1").item(0).getTextContent());
            aux.setEndereco2(element.getElementsByTagName("Address2").item(0).getTextContent());
            aux.setEndereco3(element.getElementsByTagName("Address3").item(0).getTextContent());
            aux.setEndereco4(element.getElementsByTagName("Address4").item(0).getTextContent());
            aux.setEndereco5(element.getElementsByTagName("Address5").item(0).getTextContent());
            aux.setEmail(element.getElementsByTagName("CustomerEmail").item(0).getTextContent());
            aux.setCidade(element.getElementsByTagName("City").item(0).getTextContent());
            aux.setCod_postal(element.getElementsByTagName("PostCode").item(0).getTextContent());
            aux.setPais(element.getElementsByTagName("Country").item(0).getTextContent());
            aux.setRegiao(element.getElementsByTagName("Region").item(0).getTextContent());
        }
        else if(position == 1){//endereço de entrega - AddressShipping
            aux.setPrimeiro_nome(element.getElementsByTagName("FirstName").item(1).getTextContent());
            aux.setUltimo_nome(element.getElementsByTagName("LastName").item(1).getTextContent());
            aux.setTelefone1(element.getElementsByTagName("Phone").item(1).getTextContent());
            aux.setTelefone2(element.getElementsByTagName("Phone2").item(1).getTextContent());
            aux.setEndereco1(element.getElementsByTagName("Address1").item(1).getTextContent());
            aux.setEndereco2(element.getElementsByTagName("Address2").item(1).getTextContent());
            aux.setEndereco3(element.getElementsByTagName("Address3").item(1).getTextContent());
            aux.setEndereco4(element.getElementsByTagName("Address4").item(1).getTextContent());
            aux.setEndereco5(element.getElementsByTagName("Address5").item(1).getTextContent());
            aux.setEmail(element.getElementsByTagName("CustomerEmail").item(1).getTextContent());
            aux.setCidade(element.getElementsByTagName("City").item(1).getTextContent());
            aux.setCod_postal(element.getElementsByTagName("PostCode").item(1).getTextContent());
            aux.setPais(element.getElementsByTagName("Country").item(1).getTextContent());
            aux.setRegiao(element.getElementsByTagName("Region").item(1).getTextContent());
        }
        
        return aux;
    }
    
    //Mudar - Não Pronto
    public static List<Produto> getItensPedido(String order_id){
        String out = Conexao.getConnectionOrderItems(order_id);
        NodeList items = null;
        List<Produto> produtos = new ArrayList<>();
        
        desconto_ = 0;
        frete_ = 0;
        double desconto = 0;
        double frete = 0;
        
        try{  
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            InputSource source = new InputSource();
            source.setCharacterStream(new StringReader(out)); //inseri a string em um InputSource pois o método parse estava com problemas para ler a string
            Document info = builder.parse(source);
            info.getDocumentElement().normalize(); //usei para confirmar a estrutura dos Nodes dentro da variável, é opcional
            items = info.getElementsByTagName("OrderItem"); //puxo tudo que tem a tag de pedido dentro do XML e guardo em uma lista
            
            for (int i = 0; i < items.getLength(); i++){ //deve-se percorrer a Node list de itens que foi gerada acima para pegar os elementos. P.S.: forEach não funciona com NodeList
                Node nodeItem = items.item(i);
                
                if (nodeItem.getNodeType() == Node.ELEMENT_NODE){
                    Element element = (Element) nodeItem;
                    
                    Produto aux = new Produto();
                    
                    aux.setId_produto_pedido(element.getElementsByTagName("OrderItemId").item(0).getTextContent());
                    aux.setId_dafiti(element.getElementsByTagName("ShopId").item(0).getTextContent());
                    aux.setId_pedido(element.getElementsByTagName("OrderId").item(0).getTextContent());
                    aux.setSku(element.getElementsByTagName("Sku").item(0).getTextContent());
                    aux.setSku_dafiti(element.getElementsByTagName("ShopSku").item(0).getTextContent());
                    aux.setNome(element.getElementsByTagName("Name").item(0).getTextContent());
                    //aux.setMarca(element.getElementsByTagName("Brand").item(i).getTextContent());
                    //aux.setDescricao(element.getElementsByTagName("Description").item(i).getTextContent());
                    aux.setPreco_original(element.getElementsByTagName("ItemPrice").item(0).getTextContent());
                    aux.setPreco_pago(element.getElementsByTagName("PaidPrice").item(0).getTextContent());
                    aux.setMoeda(element.getElementsByTagName("Currency").item(0).getTextContent());
                    aux.setImposto(element.getElementsByTagName("TaxAmount").item(0).getTextContent());
                    aux.setFrete(element.getElementsByTagName("ShippingAmount").item(0).getTextContent());
                    aux.setVoucher(element.getElementsByTagName("VoucherCode").item(0).getTextContent());
                    aux.setVoucher_amount(element.getElementsByTagName("VoucherAmount").item(0).getTextContent());
                    aux.setStatus(element.getElementsByTagName("ReturnStatus").item(0).getTextContent());
                    aux.setTransportadora(element.getElementsByTagName("ShipmentProvider").item(0).getTextContent());
                    aux.setTipo_envio(element.getElementsByTagName("ShippingType").item(0).getTextContent());
                    
                    
                    desconto = desconto + (Double.parseDouble(element.getElementsByTagName("ItemPrice").item(0).getTextContent()) - Double.parseDouble(element.getElementsByTagName("PaidPrice").item(0).getTextContent()));
                    frete = frete + Double.parseDouble(element.getElementsByTagName("ShippingAmount").item(0).getTextContent());
                            
                    produtos.add(aux);
                } 
            }
            
            desconto_ = desconto;
            frete_ = frete;
            
        }catch(Exception e){ //preguiça de tratar as exceções xDDDDDDD
            System.out.println(e);
        }
        
        return produtos;
    }
    
  
}
