
package Controle;

import Dados.Pedido;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

public class Tabela_Pedidos_Pendentes extends AbstractTableModel{
    SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    public static List<Pedido> pedidos_pendentes = new ArrayList<>();
    private ArrayList<String> Campos = new ArrayList<>();
    
    public Tabela_Pedidos_Pendentes() {
        Campos.add("Número Pedido");
        Campos.add("Data");
        Campos.add("Cliente");
        Campos.add("Valor");
        Campos.add("Pagamento");
        Campos.add("Itens");
        Campos.add("Status");
    }

    @Override
    public String getColumnName(int columnIndex){
        return this.Campos.get(columnIndex);
    }
    
    @Override
    public int getRowCount() {
        return pedidos_pendentes.size();
    }

    @Override
    public int getColumnCount() {
        return Campos.size();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Pedido pedido = pedidos_pendentes.get(rowIndex);
        
        switch (columnIndex) {
            case 0://Número Pedido
                return pedido.getNumero_pedido();
            case 1: 
                return pedido.getData_criacao_pedido();
            case 2://Número Pedido
                return pedido.getCliente().getNome();
            case 3://Valor
                return "R$" + pedido.getPreco();
            case 4://Pagamento
                if("braspag_cc".equals(pedido.getMetodo_pagamento())){
                    return "Cartão de Crédito";
                }
                else if("braspag_boleto".equals(pedido.getMetodo_pagamento())){
                    return "Boleto";
                }
                else{
                    return pedido.getMetodo_pagamento();
                }
            case 5://Itens
                return pedido.getQtd_itens();
            case 6:                
                if(null != pedido.getStatus()){ //Status
                    switch (pedido.getStatus()) {
                        case "delivered":
                            return "Entregue";
                        case "return_shipped_by_customer":
                            return "Retorno enviado pelo cliente";
                        case "returned":
                            return "Retornou";
                        case "shipped":
                            return "Enviado";
                        case "canceled":
                            return "Cancelado";
                        case "pending":
                            return "Pendente";
                    }
                }
            default:
                return "";
        }
    }
    
    public void LimparTabela(){
        pedidos_pendentes.removeAll(pedidos_pendentes);
        pedidos_pendentes.clear();  
        this.fireTableDataChanged();
    }

    public static List<Pedido> getPedidos_pendentes() {
        return pedidos_pendentes;
    }

    public static void setPedidos_pendentes(List<Pedido> pedidos_pendentes) {
        Tabela_Pedidos_Pendentes.pedidos_pendentes = pedidos_pendentes;
    }
    
}
