
package Controle;

import Dados.Produto;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

public class Tabela_Produtos extends AbstractTableModel{
    
    public static List<Produto> produtos = new ArrayList<>();
    private ArrayList<String> Campos = new ArrayList<>();
    
    public Tabela_Produtos() {
        Campos.add("ID");
        Campos.add("Dafiti - ID");
        Campos.add("Seller SKU");
        Campos.add("Produto");
        Campos.add("Tipo de Envio");
        Campos.add("Preço Original");
        Campos.add("Preço Pago");
        Campos.add("Preço Frete");
    }
    
    @Override
    public String getColumnName(int columnIndex){
        return this.Campos.get(columnIndex);
    }

    @Override
    public int getRowCount() {
        return produtos.size();
    }

    @Override
    public int getColumnCount() {
        return Campos.size();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Produto produto = produtos.get(rowIndex);
        
        switch (columnIndex) {
            case 0:
                return produto.getId_produto_pedido();
            case 1:{    
                return produto.getId_dafiti();
            }
            case 2:
                return produto.getSku();
            case 3://Pagamento
                return produto.getNome();
            case 4://Itens
                return produto.getTipo_envio();
            case 5:                
                return produto.getPreco_original();
            case 6:                
                return produto.getPreco_pago();
            case 7:                
                return produto.getFrete();
            default:
                return "";
        }
    }
    
    public void LimparTabela(){
        produtos.removeAll(produtos);
        produtos.clear();  
        this.fireTableDataChanged();
    }

    public static List<Produto> getProdutos() {
        return produtos;
    }

    public static void setProdutos(List<Produto> produtos) {
        Tabela_Produtos.produtos = produtos;
    }
    
}
