/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Conexao_Dafiti;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.TimeZone;
import java.util.TreeMap;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author Hericles
 */
public class SellerCenterAPI {
    private static final String ScApiHost = "https://rocket:rock4me@api.sellercenter.com.br/"; // Host de API // mantém a barra final
    private static final String HASH_ALGORITHM = "HmacSHA256";
    private static final String CHAR_UTF_8 = "UTF-8";
    private static final String CHAR_ASCII = "ASCII";

    /**
    * calculates the signature and sends the request
    *
    * @param params Map - request parameters
    * @param apiKey String - user's API Key
    * @param XML String - Request Body
    */

    public static String getSellercenterApiResponse(Map<String, String> params, String apiKey, String XML) {
        String queryString = "";
        String Output = "";
        HttpURLConnection connection = null;
        URL url = null;
        
        Map<String, String> sortedParams = new TreeMap<String, String>(params);
        queryString = toQueryString(sortedParams);

        final String signature = hmacDigest(queryString, apiKey, HASH_ALGORITHM);
        queryString = queryString.concat("&Signature=".concat(signature));
        final String request = ScApiHost.concat("?".concat(queryString));
        
        try {
            url = new URL(request);
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestProperty("charset", CHAR_UTF_8);
            connection.setUseCaches(false);

            if (!XML.equals("")) {
                //final String XML2 = "https://rocket:rock4me@api.sellercenter.com.br/?Action=GetProducts&Filter=all&Format=XML&Timestamp=2019-05-20T10%3A51%3A47-03%3A00&UserID=paulo%40pwbrasil.ind.br&Version=1.0&Signature=64eb6c2b71ccfba6a28e450d65a90879fe13dfdffbb08ca2c7c1bab7428250f3";
                connection.setRequestProperty("Content-Length", "" + Integer.toString(XML.getBytes().length));
                try (DataOutputStream wr = new DataOutputStream(connection.getOutputStream())) {
                    wr.writeBytes(XML);
                    wr.flush();
                }
            }
            String line;
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            
            while ((line = reader.readLine()) != null) {
                Output += line + "\n";
            }
            
        } catch (Exception e) {
            e.printStackTrace();
       }
        return Output;
    }
    
    //______________________________________________________________________________________________________________________
    /**
    * generates hash key
    *
    * @param msg
    * @param keyString
    * @param algo
    * @return string
    */
    private static String hmacDigest(String msg, String keyString, String algo) {
        String digest = null;
        try {
            SecretKeySpec key = new SecretKeySpec((keyString).getBytes(CHAR_UTF_8), algo);
            Mac mac = Mac.getInstance(algo);
            mac.init(key);
            final byte[] bytes = mac.doFinal(msg.getBytes(CHAR_ASCII));
            StringBuffer hash = new StringBuffer();
            
            for (int i = 0; i < bytes.length; i++) {
                String hex = Integer.toHexString(0xFF & bytes[i]);
                if (hex.length() == 1) {
                    hash.append('0');
                }
                hash.append(hex);
            }
            digest = hash.toString();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return digest;
    }
    
    //______________________________________________________________________________________________________________________
    /**
    * build querystring out of params map
    *
    * @param data map of params
    * @return string
    * @throws UnsupportedEncodingException
    */
    private static String toQueryString(Map<String, String> data) {
        String queryString = "";
        try{
            StringBuffer params = new StringBuffer();
            
            for (Map.Entry<String, String> pair : data.entrySet()) {
                params.append(URLEncoder.encode((String) pair.getKey(), CHAR_UTF_8) + "=");
                params.append(URLEncoder.encode((String) pair.getValue(), CHAR_UTF_8) + "&");
            }
            if (params.length() > 0) {
                params.deleteCharAt(params.length() - 1);
            }
            queryString = params.toString();
        } catch(UnsupportedEncodingException e){
            e.printStackTrace();
        }

        return queryString;
    }
    
    //______________________________________________________________________________________________________________________
    /**
    * returns the current timestamp
    * @return current timestamp in ISO 8601 format
    */
    
    public static String getCurrentTimestamp(){
        final TimeZone tz = TimeZone.getTimeZone("UTC");
        final DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mmZ");
        df.setTimeZone(tz);
        final String nowAsISO = df.format(new Date());
        return nowAsISO;
    }
}
