/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Conexao_Dafiti;

import static Conexao_Dafiti.SellerCenterAPI.getCurrentTimestamp;
import static Conexao_Dafiti.SellerCenterAPI.getSellercenterApiResponse;
import Controle.Controle_Exportar_Pedido;
import Controle.Controle_Pedidos;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Hericles
 */
public class Conexao {
    
    public static String getConnectionOrders(){
        Map<String, String> params = new HashMap<>();
        
        params.put("UserID", "paulo@pwbrasil.ind.br");
        params.put("Timestamp", getCurrentTimestamp());
        params.put("Version", "1.0");
        params.put("Action", "GetOrders");
        //params.put("Limit","1");/*parametros de consulta também vão nesse map*/
        
        final String apiKey = "d8404a5f5793bfe50a8e9ffbe8141bf5b67cf904";
        final String XML = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?><Request></Request>";//no XML se insere informações de cadastro, caso seja necessário
        final String out = getSellercenterApiResponse(params, apiKey, XML); 
        
        return out; 
    }
    
    public static String getConnectionOrderItems(String order_id){
        Map<String, String> params = new HashMap<>();
        
        params.put("UserID", "paulo@pwbrasil.ind.br");
        params.put("Timestamp", getCurrentTimestamp());
        params.put("Version", "1.0");
        params.put("Action", "GetOrderItems");
        params.put("OrderId", order_id);/*parametros de consulta também vão nesse map*/
        
        final String apiKey = "d8404a5f5793bfe50a8e9ffbe8141bf5b67cf904";
        final String XML = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?><Request></Request>";//no XML se insere informações de cadastro, caso seja necessário
        final String out = getSellercenterApiResponse(params, apiKey, XML); 
        
        return out; 
    }
    
//    public static void main(String[] args) {
//        //Controle.Controle_Pedidos p = new Controle_Pedidos();
//        //Controle.Controle_Pedidos.getPedidos();
//        //Controle_Pedidos.getItensPedido("4166210");
//        
//        //Controle_Exportar_Pedido.inserirPedidoERP("4311823");
//        
//        String a = "VBTSC14234235/P";
//        
//        String[] textoSeparado = a.split("/");
//        System.out.println(Arrays.toString(textoSeparado));
//
//        String j = textoSeparado[0];
//        
//        for(int i = 0; i < j.length(); i++){
//            char c = j.charAt(i);
//            
//            if(Character.isLetter(c)){// se a posição for uma letra
//                //System.out.println("String");
//            }
//            else if(Character.isDigit(c)){// se a posição for numero
//                //System.out.println("Numero");
//            }
//        }
//
//        
//        
//        
//    }
}
