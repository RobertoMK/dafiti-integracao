
package Interface;

import static Controle.Controle_Pedidos.getPedidos;
import Controle.Tabela_Pedidos_Exportados;
import Controle.Tabela_Pedidos_Pendentes;
import Dados.Pedido;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.Desktop;
import java.awt.Font;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Pedidos extends javax.swing.JFrame {
    
    Tabela_Pedidos_Pendentes tabela_pedidos_pendentes = new Tabela_Pedidos_Pendentes();
    Tabela_Pedidos_Exportados tabela_pedidos_exportados = new Tabela_Pedidos_Exportados();
    List<Pedido> pedidos = new ArrayList<>();
    Pedido pedido_selecionado = new Pedido();
    Dados_Pedido dados;

    public Pedidos() {
        initComponents();
        this.setLocationRelativeTo(null); 
        this.setResizable(false);
        
        CarregarPedidos();
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenu1 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();
        jTabbedPane3 = new javax.swing.JTabbedPane();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        btn_imprimir = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTable3 = new javax.swing.JTable();

        jMenu1.setText("jMenu1");

        jMenu2.setText("jMenu2");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jTabbedPane1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jTable1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jTable1.setModel(
            tabela_pedidos_pendentes
        );
        jTable1.setRowHeight(30);
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        jButton1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButton1.setText("Ver Pedido");
        jButton1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton1MouseClicked(evt);
            }
        });

        jButton2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButton2.setText("Exportar para o Virtual");
        jButton2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton2MouseClicked(evt);
            }
        });

        btn_imprimir.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btn_imprimir.setText("Imprimir");
        btn_imprimir.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_imprimirMouseClicked(evt);
            }
        });

        jButton4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButton4.setText("Atualizar");
        jButton4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton4MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(304, 304, 304)
                        .addComponent(jButton1)
                        .addGap(18, 18, 18)
                        .addComponent(jButton2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btn_imprimir)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 320, Short.MAX_VALUE)
                        .addComponent(jButton4)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 561, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButton2)
                    .addComponent(btn_imprimir)
                    .addComponent(jButton4))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Pedidos Pendentes", jPanel1);

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));

        jTable3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jTable3.setModel(
            tabela_pedidos_exportados
        );
        jTable3.setRowHeight(30);
        jScrollPane3.setViewportView(jTable3);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 1126, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 615, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Pedidos Exportados", jPanel2);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    //Atualizar
    private void jButton4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton4MouseClicked
        tabela_pedidos_exportados.LimparTabela();
        tabela_pedidos_pendentes.LimparTabela();
        
        CarregarPedidos();
    }//GEN-LAST:event_jButton4MouseClicked

    // Imprimir
    private void btn_imprimirMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_imprimirMouseClicked
        if(jTable1.getSelectedRowCount() != 0){
            try {
                pedido_selecionado = Tabela_Pedidos_Pendentes.getPedidos_pendentes().get(jTable1.getSelectedRow());
                GerarPDF();
            } catch (FileNotFoundException ex) {
                JOptionPane.showMessageDialog(null, "Erro ao gerar o PDF do pedido, verifique se o mesmo está aberto");
                Logger.getLogger(Pedidos.class.getName()).log(Level.SEVERE, null, ex);
            } catch (BadElementException | IOException ex) {
                Logger.getLogger(Pedidos.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else{
            JOptionPane.showMessageDialog(null, "Nenhum pedido selecionado");
        }
    }//GEN-LAST:event_btn_imprimirMouseClicked

    //Exportar para Virtual Age
    private void jButton2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton2MouseClicked
        if(jTable1.getSelectedRowCount() != 0){
        
        }
        else{
            JOptionPane.showMessageDialog(null, "Nenhum pedido selecionado");
        }
    }//GEN-LAST:event_jButton2MouseClicked

    //Ver dados do pedido
    private void jButton1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton1MouseClicked
        Pedido novo;
        
        if(jTable1.getSelectedRowCount() != 0){
            novo = Tabela_Pedidos_Pendentes.getPedidos_pendentes().get(jTable1.getSelectedRow());
            dados = new Dados_Pedido();
            dados.receberPedido(novo);
            dados.setVisible(true);
        }
        else{
            JOptionPane.showMessageDialog(null, "Nenhum pedido selecionado");
        }
    }//GEN-LAST:event_jButton1MouseClicked

    
    //selecionar pedido
    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        if(evt.getClickCount() > 1){
            jButton1MouseClicked(evt);
        }
    }//GEN-LAST:event_jTable1MouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Pedidos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Pedidos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Pedidos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Pedidos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Pedidos().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_imprimir;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton4;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTabbedPane jTabbedPane3;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTable3;
    // End of variables declaration//GEN-END:variables
    
    public void CarregarPedidos(){
        List<Pedido> aux1 = new ArrayList<>();
        List<Pedido> aux2 = new ArrayList<>();
        
        pedidos = getPedidos();
        
        for(int i = 0; i < pedidos.size(); i++){
            if("pending".equals(pedidos.get(i).getStatus())){
                aux1.add(pedidos.get(i));
            }
            else{
                aux2.add(pedidos.get(i));
            }
        }
        
        Tabela_Pedidos_Pendentes.setPedidos_pendentes(aux1);
        Tabela_Pedidos_Exportados.setPedidos_exportados(aux2);
    } 
    
    public void GerarPDF() throws FileNotFoundException, BadElementException, IOException{
        Document document = new Document();
        
        try {
            PdfWriter.getInstance(document, new FileOutputStream("documento.pdf"));
            document.open();
            document = MontarCabecalho(document);
            
        } catch (DocumentException ex) {
            System.out.println("Error:"+ex);
        }finally{
            document.close();
        }
        
        try {
            Desktop.getDesktop().open(new File("documento.pdf"));
        } catch (IOException ex) {
            System.out.println("Error:"+ex);
        }
    }
    
    public Document MontarCabecalho(Document document) throws DocumentException, BadElementException, IOException{
        document.add(new Paragraph(new Phrase(20F, "                                                Detalhes do Pedido de N° " + pedido_selecionado.getId_pedido(), FontFactory.getFont(FontFactory.TIMES_BOLD, 13F))));

        document.add(new Paragraph(new Phrase(20F, "                   ", FontFactory.getFont(FontFactory.TIMES_BOLD, 13F))));
        document.add(new Paragraph("Ped:"));
        document.add(new Paragraph("Cod:"));
        document.add(new Paragraph("Transf:"));
        document.add(new Paragraph(new Phrase(20F, "                   ", FontFactory.getFont(FontFactory.TIMES_BOLD, 13F))));
        
        //__________________________________________________________________________________________________
        
        PdfPTable tabela1 =  new PdfPTable(2);

        PdfPCell titulo1 = new PdfPCell(new Paragraph(new Phrase(20F, "Cliente", FontFactory.getFont(FontFactory.TIMES_BOLD, 13F))));
        titulo1.setHorizontalAlignment(Element.ALIGN_CENTER);
        titulo1.setColspan(1);
        tabela1.addCell(titulo1);
        titulo1 = new PdfPCell(new Paragraph(new Phrase(20F, "Endereço", FontFactory.getFont(FontFactory.TIMES_BOLD, 13F))));
        titulo1.setHorizontalAlignment(Element.ALIGN_CENTER);
        titulo1.setColspan(1);
        tabela1.addCell(titulo1);
        PdfPCell e = new PdfPCell(new Paragraph("Data: " + pedido_selecionado.getData_criacao_pedido()));
        tabela1.addCell(e);
        e = new PdfPCell(new Paragraph(pedido_selecionado.getCliente().getEnd_cobranca().getEndereco1()));
        tabela1.addCell(e);
        e = new PdfPCell(new Paragraph("Cliente: " + pedido_selecionado.getCliente().getNome()));
        tabela1.addCell(e);
        e = new PdfPCell(new Paragraph(pedido_selecionado.getCliente().getEnd_cobranca().getEndereco2()));
        tabela1.addCell(e);
        e = new PdfPCell(new Paragraph("CPF: " + pedido_selecionado.getCliente().getCpf()));
        tabela1.addCell(e);
        e = new PdfPCell(new Paragraph(pedido_selecionado.getCliente().getEnd_cobranca().getEndereco3()));
        tabela1.addCell(e);
        e = new PdfPCell(new Paragraph("Telefone 1: " + pedido_selecionado.getCliente().getEnd_cobranca().getTelefone1()));
        tabela1.addCell(e);
        e = new PdfPCell(new Paragraph(pedido_selecionado.getCliente().getEnd_cobranca().getEndereco4()));
        tabela1.addCell(e);
        e = new PdfPCell(new Paragraph("Telefone 2: " + pedido_selecionado.getCliente().getEnd_cobranca().getTelefone2()));
        tabela1.addCell(e);
        e = new PdfPCell(new Paragraph(pedido_selecionado.getCliente().getEnd_cobranca().getEndereco5()));
        tabela1.addCell(e);
        
        if("braspag_cc".equals(pedido_selecionado.getMetodo_pagamento())){
            e = new PdfPCell(new Paragraph("Pagamento: Cartão"));
            tabela1.addCell(e);
        }
        else if("braspag_boleto".equals(pedido_selecionado.getMetodo_pagamento())){
            e = new PdfPCell(new Paragraph("Pagamento: Boleto"));
            tabela1.addCell(e);     
        }
        
        e = new PdfPCell(new Paragraph(pedido_selecionado.getCliente().getEnd_entrega().getCidade() + " " + pedido_selecionado.getCliente().getEnd_cobranca().getCod_postal() + " " + pedido_selecionado.getCliente().getEnd_cobranca().getPais()));
        tabela1.addCell(e);
        
        document.add(tabela1);
        
        //____________________________________________________________________________________________________________

        document.add(new Paragraph(new Phrase(20F, "                   ", FontFactory.getFont(FontFactory.TIMES_BOLD, 13F))));

        PdfPTable tabela =  new PdfPTable(6);
        tabela.getDefaultCell().setBorder(PdfPCell.NO_BORDER);

        PdfPCell titulo = new PdfPCell(new Paragraph("Produtos"));
        titulo.setHorizontalAlignment(Element.ALIGN_CENTER);
        titulo.setColspan(6);
        tabela.addCell(titulo);
        PdfPCell c1 = new PdfPCell(new Paragraph("#"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabela.addCell(c1);
        PdfPCell c2 = new PdfPCell(new Paragraph("Nome"));
        c2.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabela.addCell(c2);
        PdfPCell c3 = new PdfPCell(new Paragraph("Seller SKU"));
        c3.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabela.addCell(c3);
        PdfPCell c4 = new PdfPCell(new Paragraph("Dafiti SKU"));
        c4.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabela.addCell(c4);
        PdfPCell c5 = new PdfPCell(new Paragraph("Preço"));
        c5.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabela.addCell(c5);
        PdfPCell c6 = new PdfPCell(new Paragraph("Preço Pago"));
        c6.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabela.addCell(c6);
        
        for(int i = 0; i < pedido_selecionado.getProdutos().size(); i++){
            PdfPCell a1 = new PdfPCell(new Paragraph(Integer.toString(i + 1)));
            a1.setHorizontalAlignment(Element.ALIGN_CENTER);
            tabela.addCell(a1);
            PdfPCell a2 = new PdfPCell(new Paragraph(pedido_selecionado.getProdutos().get(i).getNome()));
            a2.setHorizontalAlignment(Element.ALIGN_CENTER);
            tabela.addCell(a2);
            PdfPCell a3 = new PdfPCell(new Paragraph(pedido_selecionado.getProdutos().get(i).getSku()));
            a3.setHorizontalAlignment(Element.ALIGN_CENTER);
            tabela.addCell(a3);
            PdfPCell a4 = new PdfPCell(new Paragraph(pedido_selecionado.getProdutos().get(i).getSku_dafiti()));
            a4.setHorizontalAlignment(Element.ALIGN_CENTER);
            tabela.addCell(a4);
            PdfPCell a5 = new PdfPCell(new Paragraph(pedido_selecionado.getProdutos().get(i).getPreco_original()));
            a5.setHorizontalAlignment(Element.ALIGN_CENTER);
            tabela.addCell(a5);
            PdfPCell a6 = new PdfPCell(new Paragraph(pedido_selecionado.getProdutos().get(i).getPreco_pago()));
            a6.setHorizontalAlignment(Element.ALIGN_CENTER);
            tabela.addCell(a6);
        }
        
        DecimalFormat df = new DecimalFormat("###,##0.00");
        double sub_total = Double.parseDouble(pedido_selecionado.getPreco()) - Double.parseDouble(pedido_selecionado.getFrete());
        double total = Double.parseDouble(pedido_selecionado.getPreco()) - Double.parseDouble(pedido_selecionado.getDesconto());
        double desconto =  Double.parseDouble(pedido_selecionado.getDesconto());

        PdfPCell s = new PdfPCell(new Paragraph("SubTotal"));
        s.setHorizontalAlignment(Element.ALIGN_CENTER);
        s.setColspan(5);
        tabela.addCell(s);
        s = new PdfPCell(new Paragraph(String.valueOf(df.format(sub_total))));
        s.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabela.addCell(s);
        s = new PdfPCell(new Paragraph("Desconto"));
        s.setHorizontalAlignment(Element.ALIGN_CENTER);
        s.setColspan(5);
        tabela.addCell(s);
        s = new PdfPCell(new Paragraph(String.valueOf(df.format(desconto))));
        s.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabela.addCell(s);
        s = new PdfPCell(new Paragraph("Frete"));
        s.setHorizontalAlignment(Element.ALIGN_CENTER);
        s.setColspan(5);
        tabela.addCell(s);
        s = new PdfPCell(new Paragraph(String.valueOf(df.format(Double.parseDouble(pedido_selecionado.getFrete())))));
        s.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabela.addCell(s);
        s = new PdfPCell(new Paragraph("Total"));
        s.setHorizontalAlignment(Element.ALIGN_CENTER);
        s.setColspan(5);
        tabela.addCell(s);
        s = new PdfPCell(new Paragraph(String.valueOf(df.format(total))));
        s.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabela.addCell(s);

        document.add(tabela);
        
        return document;
    }
}
