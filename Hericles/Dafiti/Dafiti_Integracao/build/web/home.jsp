<%@page import="Dados.Pedido"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="org.w3c.dom.Element"%>
<%@page import="org.w3c.dom.Node"%>
<%@page import="org.w3c.dom.NodeList"%>
<%@page import="Controle.Controle_Pedidos"%>
<!DOCTYPE html>
<html lang="pt">
    <head>  
        <script>
            
            var i = setInterval(function(){
                clearInterval(i);
                
                document.getElementById("loading").style.display = "none";
                document.getElementById("conteudo").style.display = "inline";
                
            }, 2000);
            
        </script>
        
        <!-- Required meta tags -->
        <meta charset="utf-8">

        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <meta name="author" content="H�ricles Fernandes">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="./css/bootstrap.min.css">

        <!-- FontAwesome CSS -->
        <link rel="stylesheet" href="./css/all.min.css">

        <!-- Custom CSS -->
        <link rel="stylesheet" href="./css/style.css">
        <title>Gerenciar Pedidos Dafiti</title>
    </head>
    <body>
        <!-- Div carregando -->
        <div  id="loading">
            <img src="loading.gif" style="width: 150px; height: 150px; margin-left: 45%; margin-top: 15%"></img> 
            <h3 style="margin-left: 45%; margin-top: -3%">Carregando ...</h3>
        </div>

        <div class="container-fluid" id="conteudo" style="display: none">
            <div class="row">

                <!-- Div navbar -->
                <div id="content" class="w-100">
                    <nav class="navbar navbar-light navbar-expand-lg bg-light" style="margin-top: -25px">
                        <div class="col px-0" style="font-size: 24px; cursor: pointer; color: #b5b5b5">&#9776;</div>
                        <h3 style="margin-right: 36%"> Gerenciar Pedidos Dafiti </h3>
                        <button style="margin-right: 15px" type="button" class="btn btn-sm btn-danger">  Sair  </button>      
                    </nav>

                    <!-- Linha -->
                    <div class="row mx-auto">
                        <div class="col">
                            <hr class="my-0">
                        </div>
                    </div>
                    
                    <!-- <img src="certo1.png" style="width: 30px"></img> -->
                    
                </div>  
                
                <!-- Div Tabelas -->
                <div id="content2" class="w-100">
                    
                    <div>
                        <!-- Titulo -->
                        <div style=" text-align: left; margin-top: 3%; margin-bottom: 1%;">
                            <div class="col-12 col-md-9 col-lg-10">
                                <h3 style="margin-left: 53%">Pendentes</h3>
                            </div>
                        </div>
                        <div class="row mx-auto" style="margin-bottom: 20%">
                            <table class="table table-responsive-sm table-hover mt-3 mx-3 table">
                                <thead style="background-color: #C0C0C0">
                                    <tr>
                                        <th class="border-top-0" scope="col"></th>
                                        <th class="border-top-0" scope="col"></th>
                                        <th class="border-top-0" scope="col">N� Pedido</th>
                                        <th class="border-top-0" scope="col">Data / Hora</th>
                                        <th class="border-top-0" scope="col">Valor</th>
                                        <th class="border-top-0" scope="col">Pagamento</th>
                                        <th class="border-top-0" scope="col">Itens</th>
                                        <th class="border-top-0" scope="col">Status</th>
                                        <th class="border-top-0" scope="col">Impresso</th>
                                        <th class="border-top-0" scope="col"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <%
                                        
                                        List<Pedido> pedidos = Controle_Pedidos.getPedidos();
                                        
                                        int cont = 1;
                                        
                                        for (int i = (pedidos.size() - 1); i > 0; i--){ 
                                            
                                            if("pending".equals(pedidos.get(i).getStatus())){
                                                out.print("<tr>");
                                                out.print("<td>");
                                                out.print(cont + "�");
                                                out.print("</td>");
                                                out.print("<td>");
                                                out.print("<button type='button' class='btn btn-sm btn-primary'><a href='dados.jsp?numero=" + pedidos.get(i).getNumero_pedido() + "&data=" + pedidos.get(i).getData_criacao_pedido() + "&id=" + pedidos.get(i).getId_pedido() + "&nome=" + pedidos.get(i).getPrimeiro_nome() + "&sobrenome=" + pedidos.get(i).getSegundo_nome() + "&cpf=" + pedidos.get(i).getCliente().getCpf() + "&telefone1=" + pedidos.get(i).getCliente().getEnd_cobranca().getTelefone1() + "&telefone2=" + pedidos.get(i).getCliente().getEnd_cobranca().getTelefone2() + "&preco=" + pedidos.get(i).getPreco() + "&pagamento=" +  pedidos.get(i).getMetodo_pagamento() + "&nome1=" +  pedidos.get(i).getCliente().getEnd_cobranca().getPrimeiro_nome() + "&sobrenome1=" + pedidos.get(i).getCliente().getEnd_cobranca().getUltimo_nome() + "&nome2=" + pedidos.get(i).getCliente().getEnd_entrega().getPrimeiro_nome() + "&sobrenome2=" + pedidos.get(i).getCliente().getEnd_entrega().getUltimo_nome() + "&endereco1=" + pedidos.get(i).getCliente().getEnd_cobranca().getEndereco1() + "&endereco2=" + pedidos.get(i).getCliente().getEnd_cobranca().getEndereco2() + "&cidade=" + pedidos.get(i).getCliente().getEnd_cobranca().getCidade() + "&codigo_postal=" + pedidos.get(i).getCliente().getEnd_cobranca().getCod_postal() + "&country=" + pedidos.get(i).getCliente().getEnd_cobranca().getPais() + "&regiao=" + pedidos.get(i).getCliente().getEnd_cobranca().getRegiao() + "&endereco3=" + pedidos.get(i).getCliente().getEnd_entrega().getEndereco1() + "&endereco4=" + pedidos.get(i).getCliente().getEnd_entrega().getEndereco2() + "&cidade2=" + pedidos.get(i).getCliente().getEnd_entrega().getCidade() + "&codigo_postal2=" + pedidos.get(i).getCliente().getEnd_entrega().getCod_postal() + "&country2=" + pedidos.get(i).getCliente().getEnd_entrega().getPais() + "&endereco5=" + pedidos.get(i).getCliente().getEnd_cobranca().getEndereco3() + "&endereco6=" + pedidos.get(i).getCliente().getEnd_cobranca().getEndereco4() + "&endereco7=" + pedidos.get(i).getCliente().getEnd_entrega().getEndereco3() + "&endereco8=" + pedidos.get(i).getCliente().getEnd_entrega().getEndereco4() +"' style='color:#ffffff'>Ver Pedido</a></button>");
                                                out.print("</td>");
                                                out.print("<td>");
                                                out.print(pedidos.get(i).getNumero_pedido());
                                                //out.print(element.getElementsByTagName("OrderNumber").item(0).getTextContent());
                                                out.print("</td>");
                                                out.print("<td>");
                                                out.print(pedidos.get(i).getData_criacao_pedido());
                                                //out.print(element.getElementsByTagName("CreatedAt").item(0).getTextContent());
                                                out.print("</td>");
                                                out.print("<td>");
                                                out.print("R$" + pedidos.get(i).getPreco());
                                                //out.print("R$" + element.getElementsByTagName("Price").item(0).getTextContent());
                                                out.print("</td>");
                                                out.print("<td>");
                                                if("braspag_cc".equals(pedidos.get(i).getMetodo_pagamento())){
                                                    out.print("Cart�o de Cr�dito");
                                                }
                                                else if("braspag_boleto".equals(pedidos.get(i).getMetodo_pagamento())){
                                                    out.print("Boleto");
                                                }
                                                else{
                                                   out.print(pedidos.get(i).getMetodo_pagamento()); 
                                                }
                                                out.print("</td>");
                                                out.print("<td>");
                                                out.print(pedidos.get(i).getQtd_itens());
                                                out.print("</td>");
                                                out.print("<td>");
                                                out.print("Pendente");
                                                out.print("</td>");
                                                out.print("<td>");
                                                out.print("<img src='certo1.png' style='width: 30px; margin-tp: -13px; margin-left: 30px'></img>");
                                                out.print("</td>");
                                                out.print("<td>");
                                                out.print("<a class='btn btn-sm btn-info' href='/Dafiti_Integracao/ServletPedidos?id="+ pedidos.get(i).getId_pedido() +"'>Exportar Virtual</a>");
                                                //out.print("<button style='margin-left: 5px' type='button'class='btn btn-sm btn-danger'>Cancelar</button>");
                                                out.print("</td>");
                                                out.print("</tr>");
                                                
                                                cont++;
                                            }
                                        }
                                    %>
                                </tbody>
                            </table>
                        </div>    
                    </div>
                                 
                    <!-- Linha -->
                    <div class="row mx-auto" style="margin-top: -170px">
                        <div class="col">
                            <hr class="my-0">
                        </div>
                    </div>
                    
                    <div>
                        <!-- Título -->
                        <div style=" text-align: left; margin-top: 3%; margin-bottom: 1%;">
                            <div class="col-12 col-md-9 col-lg-10">
                                <h3 style="margin-left: 54%">Exportados</h3>
                            </div>
                        </div>

                        <div class="row mx-auto" style="margin-bottom: 20%">
                            <table class="table table-responsive-sm table-hover mt-3 mx-3 table">
                                <thead style="background-color: #C0C0C0">
                                    <tr>
                                        <th class="border-top-0" scope="col"></th>
                                        <th class="border-top-0" scope="col"></th>
                                        <th class="border-top-0" scope="col">N� Pedido</th>
                                        <th class="border-top-0" scope="col">Data / Hora</th>
                                        <th class="border-top-0" scope="col">Valor</th>
                                        <th class="border-top-0" scope="col">Pagamento</th>
                                        <th class="border-top-0" scope="col">Itens</th>
                                        <th class="border-top-0" scope="col">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <%
                                        List<Pedido> pedidos1 = Controle_Pedidos.getPedidos();
                                        int cont2 = 1;
                                        int contador = 1;
                                        
                                        for (int i = (pedidos1.size() - 1); i > 0; i--){ 
                                            
                                            if(contador < 100){
                                                if(!"pending".equals(pedidos1.get(i).getStatus())){
                                                    out.print("<tr>");
                                                    out.print("<td>");
                                                    out.print(cont2 + "�");
                                                    out.print("</td>");
                                                    out.print("<td>");
                                                    out.print("<button type='button' class='btn btn-sm btn-primary'><a href='dados.jsp?numero=" + pedidos1.get(i).getNumero_pedido() + "&data=" + pedidos1.get(i).getData_criacao_pedido() + "&id=" + pedidos1.get(i).getId_pedido() + "&nome=" + pedidos1.get(i).getPrimeiro_nome() + "&sobrenome=" + pedidos1.get(i).getSegundo_nome() + "&cpf=" + pedidos1.get(i).getCliente().getCpf() + "&telefone1=" + pedidos1.get(i).getCliente().getEnd_cobranca().getTelefone1() + "&telefone2=" + pedidos1.get(i).getCliente().getEnd_cobranca().getTelefone2() + "&preco=" + pedidos1.get(i).getPreco() + "&pagamento=" +  pedidos1.get(i).getMetodo_pagamento() + "&nome1=" +  pedidos1.get(i).getCliente().getEnd_cobranca().getPrimeiro_nome() + "&sobrenome1=" + pedidos1.get(i).getCliente().getEnd_cobranca().getUltimo_nome() + "&nome2=" + pedidos1.get(i).getCliente().getEnd_entrega().getPrimeiro_nome() + "&sobrenome2=" + pedidos1.get(i).getCliente().getEnd_entrega().getUltimo_nome() + "&endereco1=" + pedidos1.get(i).getCliente().getEnd_cobranca().getEndereco1() + "&endereco2=" + pedidos1.get(i).getCliente().getEnd_cobranca().getEndereco2() + "&cidade=" + pedidos1.get(i).getCliente().getEnd_cobranca().getCidade() + "&codigo_postal=" + pedidos1.get(i).getCliente().getEnd_cobranca().getCod_postal() + "&country=" + pedidos1.get(i).getCliente().getEnd_cobranca().getPais() + "&regiao=" + pedidos1.get(i).getCliente().getEnd_cobranca().getRegiao() + "&endereco3=" + pedidos1.get(i).getCliente().getEnd_entrega().getEndereco1() + "&endereco4=" + pedidos1.get(i).getCliente().getEnd_entrega().getEndereco2() + "&cidade2=" + pedidos1.get(i).getCliente().getEnd_entrega().getCidade() + "&codigo_postal2=" + pedidos1.get(i).getCliente().getEnd_entrega().getCod_postal() + "&country2=" + pedidos1.get(i).getCliente().getEnd_entrega().getPais() + "&endereco5=" + pedidos1.get(i).getCliente().getEnd_cobranca().getEndereco3() + "&endereco6=" + pedidos1.get(i).getCliente().getEnd_cobranca().getEndereco4() + "&endereco7=" + pedidos1.get(i).getCliente().getEnd_entrega().getEndereco3() + "&endereco8=" + pedidos1.get(i).getCliente().getEnd_entrega().getEndereco4() +"' style='color:#ffffff'>Ver Pedido</a></button>");
                                                    out.print("</td>");
                                                    out.print("<td>");
                                                    out.print(pedidos1.get(i).getNumero_pedido());
                                                    out.print("</td>");
                                                    out.print("<td>");
                                                    out.print(pedidos1.get(i).getData_criacao_pedido());
                                                    out.print("</td>");
                                                    out.print("<td>");
                                                    out.print("R$" + pedidos1.get(i).getPreco());
                                                    out.print("</td>");
                                                    out.print("<td>");
                                                    if("braspag_cc".equals(pedidos1.get(i).getMetodo_pagamento())){
                                                        out.print("Cart�o de Cr�dito");
                                                    }
                                                    else if("braspag_boleto".equals(pedidos1.get(i).getMetodo_pagamento())){
                                                        out.print("Boleto");
                                                    }
                                                    //out.print(element.getElementsByTagName("PaymentMethod").item(0).getTextContent());
                                                    out.print("</td>");
                                                    out.print("<td>");
                                                    out.print(pedidos1.get(i).getQtd_itens());
                                                    out.print("</td>");
                                                    out.print("<td>");
                                                    if("delivered".equals(pedidos1.get(i).getStatus())){
                                                        out.print("Entregue");
                                                    }
                                                    else if("shipped".equals(pedidos1.get(i).getStatus())){
                                                        out.print("Enviado");
                                                    }
                                                    else{
                                                        out.print("Retorno Enviado Pelo Cliente");
                                                    }
                                                    //out.print(element.getElementsByTagName("Status").item(0).getTextContent());
                                                    out.print("</td>");
                                                    out.print("</tr>");

                                                    cont2++;
                                                }
                                            }
                                            contador = contador + 1;
                                        }
                                    %>
                                </tbody>
                            </table>
                        </div>
                    </div> 
                </div>            
            </div>
        </div>

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/sidemenu.config.js"></script>

    </body>

</html>