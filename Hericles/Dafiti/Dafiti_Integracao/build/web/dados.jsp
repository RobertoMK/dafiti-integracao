<%@page import="java.util.List"%>
<%@page import="Dados.Produto"%>
<%@page import="Dados.Produto"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="org.w3c.dom.Element"%>
<%@page import="org.w3c.dom.Node"%>
<%@page import="org.w3c.dom.NodeList"%>
<%@page import="Controle.Controle_Pedidos"%>
<!DOCTYPE html>
<html lang="pt">
    <head>   
        
        <script>
            
            var i = setInterval(function(){
                clearInterval(i);
                
                document.getElementById("loading").style.display = "none";
                document.getElementById("conteudo").style.display = "inline";
                
            }, 2000);

        </script>
        
        <!-- Required meta tags -->
        <meta http-equiv="Content-Type" content="text/html" charset="utf-8">

        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <meta name="author" content="H�ricles Fernandes">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="./css/bootstrap.min.css">

        <!-- FontAwesome CSS -->
        <link rel="stylesheet" href="./css/all.min.css">

        <!-- Custom CSS -->
        <link rel="stylesheet" href="./css/style.css">
        <title>Gerenciar Pedidos Dafiti</title>
    </head>
    <body>
        
        <!-- Div carregando -->
        <div  id="loading">
            <img src="loading.gif" style="width: 150px; height: 150px; margin-left: 45%; margin-top: 15%"></img> 
            <h3 style="margin-left: 45%; margin-top: -3%">Carregando ...</h3>
        </div>
        
        <div class="container-fluid" id="conteudo" style="display: none">
            <div class="row">
                <!-- Div navbar -->
                <div id="content" class="w-100">
                    <nav class="navbar navbar-light navbar-expand-lg bg-light" style="margin-top: -25px">
                        <div class="col px-0" style="font-size: 24px; cursor: pointer; color: #b5b5b5" onclick="openSM()">&#9776;</div>
                        <%  
                            out.print("<h3 style='margin-right: 36%'>Pedido N� " + request.getParameter("numero") + "</h3>");
                        %>
                        
                        <button type="button" class="btn btn-sm btn-danger" style="margin-right: 10px;" onclick="window.print()">Imprimir Pedido</button>
                        
                    </nav>

                    <!-- Linha -->
                    <div class="row mx-auto">
                        <div class="col">
                            <hr class="my-0">
                        </div>
                    </div>
                </div>  
                
                <!-- Div Dados -->
                <div id="content2" class="w-100">
                    <div>
                        <div style=" text-align: left; margin-top: 3%; margin-bottom: 1%;">
                            <form role="form" style="margin-left: 2%; margin-right: 2%">
                                <fieldset>
                                    <h3>Dados</h3>
                                    <p></p>
                                    <div class="row">
                                        <div style="margin-left: 1.5%">
                                            <label class="font-weight-bold" for="data">Data</label>
                                            <%
                                                out.print("<input type='text' style='width: 200px' class='form-control' id='data' readonly='true' value='"+ request.getParameter("data") +"'>");
                                            %>
                                            <p></p>
                                            <label class="font-weight-bold" for="nome">Nome Cliente</label>
                                            <%
                                                out.print("<input type='text' class='form-control' id='nome' readonly='true' value='"+ request.getParameter("nome") + " " + request.getParameter("sobrenome") +"'>");
                                            %>
                                            <p></p>
                                            <label class="font-weight-bold" for="cpf">CPF</label>
                                            <%
                                                out.print("<input type='text' class='form-control' id='cpf' readonly='true' value='"+ request.getParameter("cpf") +"'>");
                                            %>
                                        </div>
                                        <div style="margin-left: 4%">
                                            <label class="font-weight-bold" for="telefone1">Telefone 1</label>
                                            <%
                                                out.print("<input type='text' style='width: 200px' class='form-control' id='telefone1' readonly='true' value='"+ request.getParameter("telefone1") +"'>");
                                            %>
                                            <p></p>
                                            <label class="font-weight-bold" for="telefone2">Telefone 2</label>
                                            <%
                                                out.print("<input type='text' class='form-control' id='telefone2' readonly='true' value='"+ request.getParameter("telefone2") +"'>");
                                            %>
                                            <p></p>
                                            <label class="font-weight-bold" for="pagamento">Pagamento</label>
                                            <%
                                                String teste3 = request.getParameter("pagamento");

                                                if("braspag_cc".equals(teste3)){
                                                    out.print("<input type='text' class='form-control' id='pagamento' readonly='true' value='Cart�o de Cr�dito'>");
                                                }
                                                else if("braspag_boleto".equals(teste3)){
                                                    out.print("<input type='text' class='form-control' id='pagamento' readonly='true' value='Boleto'>"); 
                                                }
                                            %>
                                        </div>
                                        <div style="margin-left: 4%">
                                            <label class="font-weight-bold" for="imposto">Imposto</label>
                                            <input type="text" style="width: 200px" class="form-control" id="imposto" readonly="true" value="0.00">
                                            <p></p>
                                            <%
                                                List<Produto> produtos = Controle_Pedidos.getItensPedido(request.getParameter("id"));
                                                double frete = 0;
                                                double desconto = 0;
                                                double total = 0;
                                                String voucher = null;
                                                DecimalFormat df = new DecimalFormat("###,##0.00");

                                                for (int i = 0; i < produtos.size(); i++){ //deve-se percorrer a Node list de itens que foi gerada acima para pegar os elementos. P.S.: forEach n�o funciona com NodeList
                                                    voucher = produtos.get(i).getVoucher();
                                                    frete = frete + Double.parseDouble(produtos.get(i).getFrete());
                                                    desconto = desconto + Double.parseDouble(produtos.get(i).getVoucher_amount());
                                                    //out.print(desconto + "----------------------------------------------------------------");

                                                }

                                                total = Double.parseDouble(request.getParameter("preco")) - desconto;

                                                out.print("<label class='font-weight-bold' for='imposto'>Total</label>");
                                                out.print("<input type='text' class='form-control' id='imposto' readonly='true' value='"+ df.format(total) +"'>");
                                                out.print("<p></p>");
                                                
                                                out.print("<label class='font-weight-bold' for='frete'>Frete</label>");
                                                out.print("<input type='text' class='form-control' id='frete' readonly='true' value='"+ df.format(frete) +"'>");
                                                
                                                out.print("</div>");
                                               
                                                out.print("<div style='margin-left: 4%'>");
                                                out.print("<label class='font-weight-bold' for='imposto'>Desconto</label>");
                                                out.print("<input type='text' style='width: 200px' class='form-control' id='desconto' readonly='true' value='"+ df.format(desconto) +"'>");
                                                out.print("<p></p>");
                                                
                                                out.print("<label class='font-weight-bold' for='imposto'>C�digo Voucher</label>");
                                                out.print("<input type='text' class='form-control' id='voucher' readonly='true' value='"+ voucher +"'>");
                                               
                                            %>
                                        </div>
                                    </div>
                                    
                                    <!-- Linha -->
                                    <div class="row mx-auto" style="margin-top: 40px">
                                        <div class="col">
                                            <hr class="my-0">
                                        </div>
                                    </div>
                                    
                                    <div class="row" style="margin-top: 50px; margin-left: 0.5%; margin-right: 1%">
                                        <div>
                                            <h3>Endere�o de Cobran�a</h3>
                                            <%
                                                out.print("<input type='text' style='width: 400px' class='form-control' readonly='true' value='"+ request.getParameter("nome1") + " " + request.getParameter("sobrenome1") +"'>");
                                            %>
                                            <%
                                                out.print("<input type='text' style='width: 400px' class='form-control' readonly='true' value='"+ request.getParameter("endereco1") + "'>");
                                            %>
                                            <%
                                                out.print("<input type='text' style='width: 400px' class='form-control' readonly='true' value='"+ request.getParameter("endereco2") + "'>");
                                            %>
                                            <%
                                                out.print("<input type='text' style='width: 400px' class='form-control' readonly='true' value='"+ request.getParameter("endereco5") + "'>");
                                            %>
                                            <%
                                                out.print("<input type='text' style='width: 400px' class='form-control' readonly='true' value='"+ request.getParameter("endereco6") + "'>");
                                            %>
                                            <%
                                                out.print("<input type='text' style='width: 400px' class='form-control' readonly='true' value='"+ request.getParameter("cidade") + " " + request.getParameter("codigo_postal") + " " + request.getParameter("country") +"'>");
                                            %>
                                        </div>
                                        <div style="margin-left: 40px">
                                            <h3>Endere�o de Entrega</h3>
                                            <%
                                                out.print("<input type='text' style='width: 400px' class='form-control' readonly='true' value='"+ request.getParameter("nome2") + " " + request.getParameter("sobrenome2") +"'>");
                                            %>
                                            <%
                                                out.print("<input type='text' style='width: 400px' class='form-control' readonly='true' value='"+ request.getParameter("endereco3") + "'>");
                                            %>
                                            <%
                                                out.print("<input type='text' style='width: 400px' class='form-control' readonly='true' value='"+ request.getParameter("endereco4") + "'>");
                                            %>
                                            <%
                                                out.print("<input type='text' style='width: 400px' class='form-control' readonly='true' value='"+ request.getParameter("endereco7") + "'>");
                                            %>
                                            <%
                                                out.print("<input type='text' style='width: 400px' class='form-control' readonly='true' value='"+ request.getParameter("endereco8") + "'>");
                                            %>
                                            <%
                                                out.print("<input type='text' style='width: 400px' class='form-control' readonly='true' value='"+ request.getParameter("cidade2") + " " + request.getParameter("codigo_postal2") + " " + request.getParameter("country2") +"'>");
                                            %>
                                        </div>
                                    </div>
                                </fieldset>
                        </form>
                        </div>
                        
                        <!-- Linha -->
                        <div class="row mx-auto" style="margin-top: 40px">
                            <div class="col">
                                <hr class="my-0">
                            </div>
                        </div>
                        
                        <%
                            List<Produto> produtos1 = Controle_Pedidos.getItensPedido(request.getParameter("id"));
                                        
                                        
                            out.print("<div class='row mx-auto' style='margin-top: 50px;'>");
                            out.print("<h3 style='margin-left: 2%'>Itens do Pedido ("+ produtos1.size() +")</h3>");
                            out.print("</div>");
                        %>
                        
                        <div style="margin-left: 20px; margin-right: 20px; margin-bottom: 5%">
                            <table class="table table-responsive-sm table-hover mt-3">
                                <thead style="background-color: #C0C0C0">
                                    <tr>
                                        <th class="border-top-0" scope="col">ID</th>
                                        <th class="border-top-0" scope="col">Dafiti - ID</th>
                                        <th class="border-top-0" scope="col">Seller SKU</th>
                                        <!-- <th class="border-top-0" scope="col">Dafiti SKU</th> -->
                                        <th class="border-top-0" scope="col">Produto</th>
                                        <th class="border-top-0" scope="col">Tipo de Envio</th>
                                        <th class="border-top-0" scope="col">Pre�o Original</th>
                                        <th class="border-top-0" scope="col">Pre�o Pago</th>
                                        <th class="border-top-0" scope="col">Frete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                    <%
                                        String id = request.getParameter("id");
                                        //out.print(request.getParameter("id"));
                                        List<Produto> products = Controle_Pedidos.getItensPedido(id);
                                        //out.print("Id Pedido: " + id);
                                        
                                        for (int i = 0; i < products.size(); i++){ //deve-se percorrer a Node list de itens que foi gerada acima para pegar os elementos. P.S.: forEach n�o funciona com NodeList
                                            //out.print("Produto " + (i + 1));
                                            out.print("<tr>");
                                            out.print("<td><font size=1></font>");
                                            out.print(products.get(i).getId_produto_pedido());
                                            out.print("</td>");
                                            out.print("<td>");
                                            out.print(products.get(i).getId_dafiti());
                                            out.print("</td>");
                                            out.print("<td>");
                                            out.print(products.get(i).getSku());
                                            out.print("</td>");
                                            out.print("<td>");
                                            out.print(products.get(i).getNome());
                                            out.print("</td>");
                                            out.print("<td>");
                                            out.print(products.get(i).getTipo_envio());
                                            out.print("</td>");
                                            out.print("<td>");
                                            out.print(products.get(i).getPreco_original());
                                            out.print("</td>");
                                            out.print("<td>");
                                            out.print(products.get(i).getPreco_pago());
                                            out.print("</td>");
                                            out.print("<td>");
                                            out.print(products.get(i).getFrete());
                                            out.print("</td>");
                                            out.print("</tr>");
                                            
                                        }
                                    %>
                                </tbody>
                            </table>
                        </div>
                              
                        <div class="row" style="margin-left: 85%">
                            <%
                                List<Produto> produto = Controle_Pedidos.getItensPedido(request.getParameter("id"));
                                double frete1 = 0;
                                double desconto1 = 0;
                                double total1 = 0;
                                DecimalFormat df1 = new DecimalFormat("###,##0.00");

                                for (int i = 0; i < produto.size(); i++){ 
                                    frete1 = frete1 + Double.parseDouble(produto.get(i).getFrete());
                                    desconto1 = desconto1 + Double.parseDouble(produto.get(i).getVoucher_amount());
                                }

                                total1 = Double.parseDouble(request.getParameter("preco")) - frete1;
                                out.print("<div>");
                                    out.print("<div>");
                                    out.print("<label class='font-weight-bold'>SubTotal: </label>");
                                    out.print("</div>");
                                    out.print("<div>");
                                    out.print("<label class='font-weight-bold'>Desconto: </label>");
                                    out.print("</div>");
                                    out.print("<div>");
                                    out.print("<label class='font-weight-bold'>Frete: </label>");
                                    out.print("</div>");
                                    out.print("<div>");
                                    out.print("<label class='font-weight-bold'>TOTAL: </label>");
                                    out.print("</div>");
                                out.print("</div>");
                                
                                out.print("<div style='margin-left: 6%;'>");
                                    out.print("<div>");
                                    out.print("<label class='font-weight'>"+ df1.format(total1) +"</label>");
                                    out.print("</div>");
                                    out.print("<div>");
                                    out.print("<label class='font-weight'>"+ df1.format(desconto1) +"</label>");
                                    out.print("</div>");
                                    out.print("<div>");
                                    out.print("<label class='font-weight'>"+ df1.format(frete1) +"</label>");
                                    out.print("</div>");
                                    out.print("<div>");
                                    out.print("<label class='font-weight'>"+ df1.format((total1 - desconto1)+ frete1) +"</label>");
                                    out.print("</div>");
                            %>
                        </div>
                    </div>
                </div>            
            </div>
        </div>

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/sidemenu.config.js"></script>
    </body>

</html>